/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for UtilityController.
 */
@isTest
private class TestConfigUtilController 
{    
 public static testmethod void runAllTest() {
        test.startTest();
        
      /*  akritiv__SysConfig_Singlec__c sysConfig = akritiv__SysConfig_Singlec__c.getOrgDefaults();
       
        if(sysConfig != null) {
           
            sysConfig.akritiv__Billing_Statement_Pdf_Template__c = 'TestPdfTemplateName';
            sysConfig.akritiv__Billing_Statement_Excel_Template__c = 'TestExcelTemplateName';
            sysConfig.akritiv__Org_Logo__c = 'Some Url to image';
            sysConfig.akritiv__Billing_Statement_PDF_page__c = 'Test';
            sysConfig.akritiv__NameSpace__c = 'testName';
            sysConfig.akritiv__Default_Currency__c = '$';
            sysConfig.akritiv__Billing_Statement_PDF_page__c = 'testPdfPage';
            sysConfig.akritiv__Billing_Statement_Excel_page__c = 'testExcelPage';
            sysConfig.akritiv__Batch_Job_Notification_Email__c = 'test@test.com';
            update sysConfig;
        }*/
        ConfigUtilController.getBatchJobNotificationEmailIds();
        ConfigUtilController.getAfterInsertFullDataLoadBatch();
        ConfigUtilController.getTriggerConfigObj();
        test.stopTest();
    }
}