/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for Closing batch processes automatically.
 */
global class AutomatedBatchCloseProcessTest implements Database.Batchable<SObject>, Database.Stateful, Schedulable  {
    global Double closetotal = 0;
    global Double opentotal= 0;
    global string lastid ='0';
    global String latestbatch;
    global String sourcesystem = 'SAP';
    global Set<Id> txIds = new set<Id>();
    global Id batchInfoId;
    global akritiv__Data_Load_Batch__c batchInfo;
    public Boolean flag = false;
    public Exception  ex;
    public Long timesInMillis = System.currentTimeMillis();
    // Global Constructor use to select Transaction with latest Batch# and Source System
    global AutomatedBatchCloseProcessTest(){
        
        /*Transaction__c tx = [ Select a.Transaction_Key__c, a.Source_System__c, a.Batch_Number__c From Transaction__c a where Source_System__c  <> null and Source_System__c <>'' and Batch_Number__c  <> null and Batch_Number__c <>'' order by Batch_Number__c desc limit 1];
        latestbatch = tx.Batch_Number__c;
        sourcesystem = tx.Source_System__c;*/
       // latestbatch = '';
        
    }

    // start method implementation of Bachable
    global Database.Querylocator start(Database.BatchableContext BC) {
    String query = '';
             try{
            //*batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c , d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.AR_RiskFactor__c, d.ARClose__c, d.ARClose_Start_Time__c, d.ARClose_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
            if(batchInfoId != null){
            System.debug('----batchInfoId----'+batchInfoId );
            batchInfo = [ Select d.akritiv__Source_System__c, d.Id, d.akritiv__Batch_Number__c , 
                        d.akritiv__Aging_Job_Status__c, d.akritiv__Aging_Job_Start_Time__c, 
                        d.akritiv__Aging_Job_End_Time__c , d.akritiv__Percentage_Close__c , d.akritiv__Batch_Job_Status__c, 
                        d.akritiv__Batch_Job_Start_Time__c, d.akritiv__Batch_Job_End_Time__c  
                        From akritiv__Data_Load_Batch__c d 
                        where Id=:batchInfoId];  
                          
                        batchInfo.akritiv__Batch_Job_Start_Time__c = System.now();
                        update batchInfo;
            
            }
            String capacity = '1';
           // if(ConfigUtilController.getBatchCapacity() != null)
            //    capacity = ConfigUtilController.getBatchCapacity();
            /** #HOS-61 **/
          //  String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
            //return Database.getQueryLocator('select id from ' +namespace+ 'Transaction__c limit ' + ConfigUtilController.getBatchCapacity() );
            
            
         //   if( ConfigUtilController.isBucketOnBase())        
                query = 'select Id from akritiv__Transaction__c LIMIT 60';
         //   else 
          //      query = 'select akritiv__Balance__c,akritiv__Source_System__c,akritiv__Base_Balance__c,akritiv__Batch_Number__c, Id from akritiv__Transaction__c where akritiv__Balance__c !=0 and akritiv__Source_System__c = \''+ sourcesystem +'\' order by Id';
          //  System.debug('Enter into START');
          } catch(Exception e){
                    sendMail();
            }
          return Database.getQueryLocator(query);
         
            
            
       
    }   
    // Method to close Transaction with Balance zero for previous batch
    // Return Type - Void 
    // Parameters - BC Database.Batchablecontext , List<SObject> scope
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {


        //if ( txIds == null ) {

        //    txIds = new Set < Id >();
        //}
        
        //List<akritiv__Transaction__c> Txs = new List<akritiv__Transaction__c>();
        // select 10000 Transactions and calculate total open balance and total closebalance value
        List<akritiv__Transaction__c> Txs = scope;
        Boolean bucketOnBase = true;        
        System.debug('---bucketOnBase---'+bucketOnBase);
        System.debug('---sourcesystem---'+sourcesystem);
      //  System.debug('---akritiv__Source_System__c---'+akritiv__Source_System__c);
        System.debug('---timesInMillis---'+timesInMillis);
        try {  
        if(bucketOnBase)
            Txs = [select akritiv__Balance__c,akritiv__Source_System__c,akritiv__Base_Balance__c,
                    akritiv__Batch_Number__c, Id 
                    from akritiv__Transaction__c 
                    where  akritiv__Base_Balance__c !=0 and akritiv__Source_System__c = :sourcesystem 
                    and ( akritiv__Last_Batch_Job_ID__c = null OR  akritiv__Last_Batch_Job_ID__c < :timesInMillis ) 
                    limit 5000];
        else 
            Txs = [select akritiv__Balance__c,akritiv__Source_System__c,akritiv__Base_Balance__c,akritiv__Batch_Number__c, Id 
                    from akritiv__Transaction__c 
                    where  akritiv__Balance__c !=0 and akritiv__Source_System__c = :sourcesystem 
                    and (  akritiv__Last_Batch_Job_ID__c = null OR  akritiv__Last_Batch_Job_ID__c < :timesInMillis ) 
                    limit 5000];
        } catch( QueryException e ) {
            flag = true;
            ex = e;
            throw e;
        }
     
       //if(bucketOnBase)
       //      Txs = [select akritiv__Balance__c,akritiv__Source_System__c,akritiv__Base_Balance__c,akritiv__Batch_Number__c, Id from akritiv__Transaction__c where  akritiv__Base_Balance__c !=0 and Id not in :txIds order by Id limit 10000];
       //else 
       //     Txs = [select akritiv__Balance__c,akritiv__Source_System__c,akritiv__Base_Balance__c,akritiv__Batch_Number__c, Id from akritiv__Transaction__c where  akritiv__Balance__c !=0 and Id not in :txIds order by Id limit 10000];
        
        //system.debug('----Txs---'+Txs);
        //system.debug('----Txs---'+Txs.size());
      
            /////////////////
            System.debug('Enter in to TRY'+Txs);
            //String str;
            //str.length();
            /////////////////
         try {    
            if(Txs.size() > 0){
               for(akritiv__Transaction__c tx : Txs) {
                   if(tx.akritiv__Source_System__c == sourcesystem){
                        txIds.add(tx.Id);
                        System.debug('---latestbatch---'+latestbatch);
                        System.debug('---tx.akritiv__Batch_Number__c---'+tx.akritiv__Batch_Number__c);
                        
                        if(tx.akritiv__Batch_Number__c != null && latestbatch != null && tx.akritiv__Batch_Number__c == latestbatch ) {
                            
                            
                            if(bucketOnBase){
                                if(tx.akritiv__Base_Balance__c != null){
                                     opentotal+=tx.akritiv__Base_Balance__c;
                                }
                            }
                            else 
                                opentotal+=tx.akritiv__Balance__c;
                        }else{
                        
                            if(bucketOnBase){
                               if(tx.akritiv__Base_Balance__c != null)
                                     closetotal +=tx.akritiv__Base_Balance__c;
                            }    
                             else 
                                closetotal += tx.akritiv__Balance__c;
                        }
                        lastid = tx.id;
                    }
                     tx.akritiv__Last_Batch_Job_ID__c = timesInMillis;
              }
              update Txs;
              
          }
        } catch(Exception e) {
            System.debug('Enter into Catch');
            flag = true;
            ex = e;
            throw e;
        }
    
    
    }
    // Methods sends email to user after transaction are closed 
    // Return Type - Void 
    // Parameter - BC Database.Batchablecontext
    public void sendMail()
    {
            
          //  String address = ConfigUtilController.getBatchJobNotificationEmailIds();
            String address = ConfigUtilController.getBatchJobNotificationEmailId();
            String[] toAddress = address.split(',');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            //String toAddress = ConfigUtilController.getBatchAutoCloseEmailService();
            //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setToAddresses(new String[] {toAddress});
           
            mail.setToAddresses(toAddress);            
            mail.setSenderDisplayName('Batch Processing :');
            // set subject
            mail.setSubject( 'SYSTEM AR Close Process Aborted : '+Userinfo.getOrganizationName());
            String mailBodyText = '<div style="font-family:Arial, Helvetica, sans-serif;font-size:16px;font-weight:bolder;color:#56c240">Notification From Akritiv</div> <br />';
          
            
            
            mailBodyText += ' <table style="background-color:#336799;color:#FFF;border:#00045a medium solid;font-family:Arial, Helvetica, sans-serif;font-size:14px;width:700px">';
            mailBodyText += '<tr >';
            
            mailBodyText += '<td width="177" style="font-weight:bolder;">Organization Name</td>';
            mailBodyText += '<td width="12" style="font-weight:bold;">:</td>';
            mailBodyText += '<td width="489" style="font-weight:bold;color:#eae835">'+Userinfo.getOrganizationName()+'</td>';
            mailBodyText += '</tr>';
             mailBodyText += '<td width="177" style="font-weight:bolder;">Organization ID</td>';
            mailBodyText += '<td width="12" style="font-weight:bold;">:</td>';
            mailBodyText += '<td width="489" style="font-weight:bold;color:#eae835">'+UserInfo.getOrganizationId()+'</td>';
            mailBodyText += '</tr>';
            mailBodyText += '<tr>';
            mailBodyText += '<td style="font-weight:bold">Date</td>';
            mailBodyText += '<td style="font-weight:bold">:</td>';
            mailBodyText += '<td>'+ Date.today() +'</td>';
            mailBodyText += '</tr>';
            mailBodyText += '</table>  <br />';
        
            mailBodyText +='<div style="font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bolder">EXCEPTION</div>';
            mailBodyText +='<br />';
            
            mailBodyText +='<table style="background-color:#fcffd2;color:#000;border:#dbb138 medium solid;font-family:Arial, Helvetica, sans-serif;font-size:12px;width:700px;vertical-align:top" cellpadding="5">';
            
            mailBodyText +='<tr>';
            mailBodyText +='<td width="170" style="font-weight:bold">Cause</td>';
            mailBodyText +='<td width="4" style="font-weight:bold">:</td>';          
            mailBodyText +='<td width="480" >'+ex.getCause()+'</td>';
            mailBodyText +='</tr>';
            mailBodyText +='<tr>';
            mailBodyText +='<td style="font-weight:bold">Line Number</td>';
            mailBodyText +='<td style="font-weight:bold">:</td>';
            mailBodyText +='<td style="color:#F00;font-weight:bold">'+ex.getLineNumber()+'</td>';
            mailBodyText +='</tr>';
            mailBodyText +='<tr>';
            mailBodyText +='<td style="font-weight:bold">Message</td>';
            mailBodyText +='<td style="font-weight:bold">:</td>';
            mailBodyText +='<td>'+ex.getMessage()+'</td>';
            mailBodyText +='</tr>';
            mailBodyText +='<tr>';
            mailBodyText +='<td style="font-weight:bold">Stack Trace String</td>';
            mailBodyText +='<td style="font-weight:bold">:</td>';
            mailBodyText +='<td>'+ex.getStackTraceString()+'</td>';
            mailBodyText +='</tr>';
            mailBodyText +='<tr>';
            mailBodyText +='<td style="font-weight:bold">Type Name</td>';
            mailBodyText +='<td style="font-weight:bold">:</td>';
            mailBodyText +='<td>'+ex.getTypeName()+'</td>';
            mailBodyText +='</tr>';        
            mailBodyText +=' </table><br>';
            
            mailBodyText +='<div style="font-family:Arial, Helvetica, sans-serif;font-size:14px;font-weight:bolder">IT</div>';
            mailBodyText +='<br />';
            
            mailBodyText +='<table style="background-color:#fcffd2;color:#000;border:#dbb138 medium solid;font-family:Arial, Helvetica, sans-serif;font-size:12px;width:700px;vertical-align:top" cellpadding="5">';        
            mailBodyText +='<tr>';
            mailBodyText +='<td style="font-weight:bold">Batch#</td>';
            mailBodyText +='<td style="font-weight:bold">:</td>';
            mailBodyText +='<td>'+latestbatch+'</td>'; 
            mailBodyText +='</tr>';
            mailBodyText +='<tr>';
            mailBodyText +='<td style="font-weight:bold">Source system#</td>';
            mailBodyText +='<td style="font-weight:bold">:</td>';
            mailBodyText +='<td>'+sourcesystem+'</td>';
            mailBodyText +='</tr>';
            
            mailBodyText +='</table>';   
             
            mail.setHtmlBody(mailBodyText);
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
            
            System.debug('Exit Catch Block' +results);    
    }
    global void finish(Database.BatchableContext BC) {
        if(flag){
            System.debug('Mail Send In Finish');
            sendMail();           
        }
        Integer dataLoadBatchSize = 200;
        // set batch size value
        akritiv__BatchJobConfiguration_Singlec__c batchConfigObj = akritiv__BatchJobConfiguration_Singlec__c.getOrgDefaults();
        if(batchConfigObj != null) {
            dataLoadBatchSize = (batchConfigObj.akritiv__DataLoad_Batch_Size__c != null && batchConfigObj.akritiv__DataLoad_Batch_Size__c > 0 && batchConfigObj.akritiv__DataLoad_Batch_Size__c < 201 ? batchConfigObj.akritiv__DataLoad_Batch_Size__c.intValue() : 200);
        }
        // calculate and set ratio value for close total balance 
        Decimal opentotalInDec = Decimal.ValueOf(opentotal).setscale(2);
      //  Decimal ratio = Math.round((opentotalInDec/100)*ConfigUtilController.getBatchAutoCloseRiskRatio());
      Long l2 = ((opentotalInDec/100)*15).longvalue();
        
        Decimal actualRiskFactor = 0;
        Decimal closetotalInDec=0;
        
        if ( closetotal != 0 && opentotal != 0 ) {
             closetotalInDec = Decimal.ValueOf(closetotal).setscale(2);
               actualRiskFactor = (closetotalInDec/opentotalInDec)*100;
                       system.debug('------actualRiskFactor----'+actualRiskFactor );
               if ( actualRiskFactor != null ) {
               
                   actualRiskFactor = Math.round(actualRiskFactor  );
               }
        }
        
        Long l1 = closetotalInDec.longvalue();
       // long l2 = ratio.longvalue();
        system.debug('------actualRiskFactor----'+actualRiskFactor );
        if ( l1 > l2 ) {
            
            AsyncApexJob a = null;
            if(BC != null) {
                a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
            }
            // set and send values with Email of Close Transactions  
            
           // String currencyIsoCode = AkritivConstants.CURRENCY_USD_CODE;
            String currencyIsoCode = 'USD';
            String opentotals = UtilityController.formatDoubleValue(currencyIsoCode , opentotal);
            opentotals = opentotals.replace('$','');
            String closetotals= UtilityController.formatDoubleValue(currencyIsoCode , closetotal);
            closetotals = closetotals.replace('$','');
            String actualRiskFactors = UtilityController.formatDoubleValue('', Double.ValueOf(actualRiskFactor));
            actualRiskFactors = actualRiskFactors.replace('$', '');
        
            // Retrive the email address from custom setting to send notification mail ( System config batch job notification emails )
            
            String addresses = ConfigUtilController.getBatchJobNotificationEmailIds();
          
            String[] toAddress  = addresses.split(',');
            
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setToAddresses(toAddress);
           // mail.setReplyTo(address);
            mail.setSenderDisplayName('Batch Processing :');
       
        // added OR condition which is used for cutstomer whose Org currency decimal seprator is comma rather then the dot.
        // ie. In EUR ( German(Germany,EURO) User Locale) in which 0.00 value is treated as 0,00 so OR codition is help us to generate valid Abort Email
        if(actualRiskFactors.trim() == '0.00' || actualRiskFactors.trim() == '0,00' ){        
            mail.setSubject( 'Aborting closing Process due to Zero Open Balance' + ' For '+ Userinfo.getOrganizationName()); 
        }else{
                mail.setSubject( 'Auto Close Risk Factor ='+ actualRiskFactors + ' For '+ Userinfo.getOrganizationName());
        }           
        
            // Send the mail with body using template (SYSTEM Risk Factor Notification)          
            String riskfactorTemplateName = 'SYSTEM Risk Factor Notification';
            List<EmailTemplate> template = new List<EmailTemplate>();
            akritiv__SysConfig_Singlec__c sysConfig =   akritiv__SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
            
            // if is akritiv org is checked in custom setting( System config ) then this template must be in the akritiv folder. 
            
            if(sysConfig.akritiv__is_Akritiv_Org__c){
                template = [ Select e.Name, e.HtmlValue From EmailTemplate e where Folder.Name like 'Akritiv%' and e.Name =:riskfactorTemplateName ];
            }else{
                template = [ Select e.Name, e.HtmlValue From EmailTemplate e where e.Name =:riskfactorTemplateName ];
            }
            if(template.size()>0){

                String templateBody = template.get(0).HtmlValue;
                
                templateBody = templateBody.replace('{!riskfactorlimit}', 15+'');
                templateBody = templateBody.replace('{!notificationdate}', Date.today()+'');
                
                 // added OR condition which is used for cutstomer whose Org currency decimal seprator is comma rather then the dot.
                 // ie. In EUR ( German(Germany,EURO) User Locale) in which 0.00 value is treated as 0,00 so OR codition is help us to generate valid Abort Email
                 if(opentotals.trim() == '0.00' || opentotals.trim() == '0,00' ){
                
                    templateBody = templateBody.replace('{!balanceopen}', opentotals+'');
                    templateBody = templateBody.replace('{!balancetobeclosed}',closetotals+'' );
                    templateBody = templateBody.replace('Auto close process aborted due to Auto Close Risk Factor' , 'Auto Close Process aborted');
                    templateBody = templateBody.replace('{!actualfactor} > {!batchautocloseriskratio}','Aborting Closing Process due to Zero Open Balance');
                    
                }
                else{
                    templateBody = templateBody.replace('{!balanceopen}', opentotals+'');
                    templateBody = templateBody.replace('{!balancetobeclosed}',closetotals+'' );
                    templateBody = templateBody.replace('{!actualfactor}',actualRiskFactors+'' );
                    templateBody = templateBody.replace('{!batchautocloseriskratio}',15+'' );
                    
                }
                /*templateBody = templateBody.replace('{!balancetobeclosed}',closetotals+'' );
                templateBody = templateBody.replace('{!actualfactor}',actualRiskFactors+'' );
                templateBody = templateBody.replace('{!batchautocloseriskratio}',ConfigUtilController.getBatchAutoCloseRiskRatio()+'' );*/
                templateBody = templateBody.replace('{!batchnumber}',latestbatch );
                if(sourceSystem == null){
                   sourceSystem = 'Not Specified'; 
                }
                templateBody = templateBody.replace('{!sourcesystem}',sourceSystem );
                templateBody = templateBody.replace('{!organizationname}',Userinfo.getOrganizationName() );
                   
                mail.setHtmlBody(templateBody);
            
            }

            /*mail.setPlainTextBody(mailBodyText);*/
            
            // update batch info record(data load batch record) with below detail.
            if ( batchInfo != null ) {
            
            
                batchInfo.akritiv__Batch_Job_Status__c = 'Aborted';
                batchInfo.akritiv__Percentage_Close__c = Math.round(actualRiskFactor)+'';
                batchInfo.akritiv__Batch_Job_End_Time__c = System.now();
                update batchInfo;
            
            }
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }else{
                
                    if(batchInfo != null){
                        batchInfo.akritiv__Percentage_Close__c = actualRiskFactor+'';  
                        update batchInfo;
                    }
                    
                    // send mail to the email address defined in custom seting( Batch job configuration) which is email address of email service (Data Load Batch Job Kick Off)
                    //notify system of this change
                    
                     String addresses = ConfigUtilController.getAutoCloseServiceNotification();
          
                     String[] toAddress  = addresses.split(',');
                    
                    //notify system of this finish
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                        
                    mail.setToAddresses(toAddress);
                 //   mail.setReplyTo(toAddress);
                    mail.setSenderDisplayName('Batch Processing :');
                    // set subject
                    mail.setSubject( 'AR Close Process');
                    String mailBodyText = '';
                    
                    if (batchInfo != null ){
                        // set body value
                        mailBodyText ='<END>'+latestbatch+''+Userinfo.getOrganizationId()+''+sourcesystem+''+Userinfo.getOrganizationId()+''+batchInfo.Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                   
            
                        
                    }else {
                        // set body value
                        mailBodyText ='<END>'+latestbatch+''+Userinfo.getOrganizationId()+''+sourcesystem+''+Userinfo.getOrganizationId()+''+'NOBATCHINFO'+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                   
            
                    }
                    
            
                    mail.setPlainTextBody(mailBodyText);
               
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        }
    }
    // Methods execute Automatically Close Batch Process 
    // Return Type - Void 
    // Parameter - sc SchedulableContext
    global void execute(SchedulableContext sc) {
    
        //get max batch num and it's source system
        try {

           List<akritiv__Transaction__c> tx = [ Select a.akritiv__Transaction_Key__c, a.akritiv__Source_System__c, 
                                               a.akritiv__Batch_Number__c 
                                               From akritiv__Transaction__c a 
                                               where akritiv__Transaction_Key__c != null and akritiv__Source_System__c  <> null and 
                                               akritiv__Source_System__c <>'' and akritiv__Batch_Number__c  <> null 
                                               and akritiv__Batch_Number__c <>'' 
                                               order by akritiv__Batch_Number__c desc limit 1];

            AutomatedBatchCloseProcessTest bafl = new AutomatedBatchCloseProcessTest();
            bafl.latestbatch = tx.get(0).akritiv__Batch_Number__c;
            bafl.sourcesystem = tx.get(0).akritiv__Source_System__c;
            Database.executeBatch(bafl, 1);

        } catch( Exception e ) {

            
        }


    }
}