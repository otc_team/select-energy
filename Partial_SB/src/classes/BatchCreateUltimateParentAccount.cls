/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This Batch class is used for creating Ultimate/Super Parent Account
 */
global class BatchCreateUltimateParentAccount implements Database.Batchable<SObject>, Database.Stateful, Schedulable  {
    
    public Exception  ex;
    //public Long timesInMillis = System.currentTimeMillis();
    
    // Global Default Constructor
    global BatchCreateUltimateParentAccount(){
        // ToDos: 
    }

    // start method implementation of Bachable to read the Child Accounts
    // Parameters - BC Database.Batchablecontext
    global Database.Querylocator start(Database.BatchableContext BC) {
      
      String query = '';
        try{
            
          String capacity = '1';
            
            String Source = 'TEST';
            
            query = 'Select Id, Name, AccountNumber, akritiv__Source__c,akritiv__Account_Key__c, Parent_Company_No__c, ParentId, Is_Parent__c from Account ';
            query += ' Where Is_Parent__c = true and Parent_Company_No__c != null and akritiv__Source__c=\'TEST\'';
            
         
         } catch(Exception e){
                  
        }
        
        system.debug('---Query--->> '+query);
        return Database.getQueryLocator(query);
        
    }
       
    
    global void execute(Database.BatchableContext BC, List<Sobject> sObjs) {
  
        List<Account>  accSetList = new List<Account>();
        Set<String> parentbinSet = new Set<String>(); 
        try{
        
          for(Sobject accSobject : sObjs) {
              Account act = (Account) accSobject;
              
               if ( act.Parent_Company_No__c != null ){

                  parentbinSet.add(act.Parent_Company_No__c);
                  accSetList.add(act);
              }
              
          }
          system.debug('======accSetList'+accSetList);
          system.debug('----> parentbinSet:'+ parentbinSet);
          Map<String,Account> mapCreateAcc = new Map<String,Account>();
          for(Account a : accSetList){
              mapCreateAcc.put(a.Parent_Company_No__c,a);
          }
          system.debug('----> mapCreateAcc:'+ mapCreateAcc);
          system.debug('----> mapCreateAcc:'+ mapCreateAcc.size());
          
          List<Account> accList = new List<Account>();
          
          List<Account> accountinsert = new List<Account>();
          Map<String,Account> mspKeyAcc = new Map<String,Account>();
          List<Account> parentBINACCONTS = [Select Id, Name, akritiv__Account_Key__c, ParentId, AccountNumber, Parent_Company_No__c,Is_Parent__c,akritiv__Source__c,akritiv__Batch_Number__c
                                    from Account where akritiv__Source__c='TEST'];
            for(Account a : parentBINACCONTS ){
                system.debug('a.akritiv__Account_Key__c========='+ a.akritiv__Account_Key__c);
                mspKeyAcc.put(a.akritiv__Account_Key__c,a);
            }
          
          for(String str : parentbinSet){
              system.debug('----> str  ---------:'+ str );
              if(!mspKeyAcc.containsKey(str)){
                  System.debug('==mapCreateAcc.get(str)=='+mapCreateAcc.get(str));
                  accList.add(mapCreateAcc.get(str));
          }                         
          } 
          system.debug('----> accList :'+ accList);
          Map<String ,List <Account > > accountParentMap = new Map<String ,List <Account >>();
          
         
      
      system.debug('----> parentBINACCONTS :'+ parentBINACCONTS.size());
      Map<Id,Account> mapAccParent = new Map<Id,Account>();
      
   
      
              for(Account accSobject : accList) {
            
            
           
           
             
             
              //create new
                    Account newAcc = new Account();
                    newAcc.Name = accSobject.Name;
                    newAcc.AccountNumber = accSobject.AccountNumber;
                    newAcc.Is_Parent__c = true;
                    newAcc.akritiv__Account_key__c = accSobject.AccountNumber;
                    newAcc.akritiv__Source__c = accSobject.akritiv__Source__c;
                    newAcc.akritiv__Batch_Number__c = accSobject.akritiv__Source__c + DateTime.now().format('ddMMyyyyhhmmss');
                    mapAccParent.put(accSobject.id,newAcc);
                    accountinsert.add(newAcc);   
                    
              
            }
           system.debug('----> mapAccParent :'+ mapAccParent);
           system.debug('----> accountinsert :'+ accountinsert);
          /* if ( accountinsert.size() > 0 ) {

              insert accountinsert;
          } 
          
          
          List<Account> updateList  = new List<Account>();
             for(Account acc : accList) {
                 acc.parentid = mapAccParent.get(acc.id).id;
                 updateList.add(acc);
             }
            system.debug('----> updateList :'+ updateList);
         
          if ( updateList.size() > 0 ) {

              update updateList;
          } */
          
          
        }
          
        catch(Exception e){
                 
        }
    
   }
   
    global void finish(Database.BatchableContext BC) {
      // Todos: update final records 
    }
    
    // This method invoke schedualbleContext. The batch return in this method will be called automatically.  
    // Return Type - Void 
    // Parameter - sc SchedulableContext
    global void execute(SchedulableContext sc) {
    
        //get max batch num and it's source system
        try {

            BatchCreateUltimateParentAccount bafl = new BatchCreateUltimateParentAccount();
            Database.executeBatch(bafl, 200);

        } catch( Exception e ) {
            
        }


    }
}