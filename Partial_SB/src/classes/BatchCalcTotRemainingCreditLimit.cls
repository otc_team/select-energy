/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This Batch class is used for creating Ultimate/Super Parent Account
 */
global class BatchCalcTotRemainingCreditLimit implements Database.Batchable<SObject>, Database.Stateful, Schedulable  {
    
    public Exception  ex;
    //public Long timesInMillis = System.currentTimeMillis();
    
    // Global Default Constructor
    global BatchCalcTotRemainingCreditLimit(){
        // ToDos: 
    }

    // start method implementation of Bachable to read the Child Accounts
    // Parameters - BC Database.Batchablecontext
    global Database.Querylocator start(Database.BatchableContext BC) {
      
      String query = '';
        try{
            
          String capacity = '1';
            
            String Source = 'TEST';
            
            //query = 'Select Id, Name, AccountNumber, akritiv__Source__c,akritiv__Account_Key__c, Parent_Company_No__c, ParentId, Is_Parent__c from Account ';
            //query += ' Where Is_Parent__c = true and ParentId != null order by ParentId';
            
            String accid='001L000000w5vGq'; //001L000000w5vGq,001L000000w5vbH  and id = :accid
            
            query = 'Select Id, Name, AccountNumber, akritiv__Source__c,akritiv__Account_Key__c, Parent_Company_No__c, ParentId, Is_Parent__c,Total_Remaining_Credit_Limit__c from Account ';
            query += ' Where Is_Ultimate_Parent__c = true and ParentId = null  ';
           
         } catch(Exception e){
                  system.debug('Error in Start Batch BatchCalcTotRemainingCreditLimit....'+e.getStackTraceString());
        }
        
        system.debug('---Query--->> '+query);
        return Database.getQueryLocator(query);
        
    }
       
    
    global void execute(Database.BatchableContext BC, List<Sobject> sObjs) {
  
         system.debug('---start of execute--->> ');
        // Total Remaining Credit Limt Calculation. 
      
        Account acc; 
        Decimal sumOfRemCreaditLimit=0.00;
        
        for(sObject accObj : sObjs) {  
            acc = (Account)accObj;
            System.debug('account id is ....'+acc.id);
        	sumOfRemCreaditLimit = getAllChildrenAccount(acc.id); 
            acc.Total_Remaining_Credit_Limit__c = sumOfRemCreaditLimit;
            update acc;
        }
   }
    
    // Method : getAllChildrenAccount
    // return type : Decimal
    // Sum of remaining credit for child accounts.
    public Decimal getAllChildrenAccount(Id accountId)
    {  
        //Account[] allChildren = new Account[] {};
        Set<Id> parentIds = new Set<Id>{accountId};
        Account[] children;
        Decimal sumOfRemCreaditLimit=0.00;
        
        try{
            do {
                children = [Select id,parentId,akritiv__Account_Key__c,akritiv__Credit_Remaining__c from Account where Is_Parent__c = true and ParentId in :parentIds];
                //allChildren.addAll(children);
                parentIds.clear();
                if(children != null && children.size() > 0)
                {
                    for (Account child : children)
                    {
                        parentIds.add(child.Id);
                        if(child.akritiv__Credit_Remaining__c != null)
                        {
                            sumOfRemCreaditLimit += child.akritiv__Credit_Remaining__c;
                        }
                    }
                }
            } while (children.size() > 0); 
        }
        catch(Exception e1){
              system.debug('Error in getAllChildrenAccount....'+e1.getStackTraceString());    
        }
        
        return sumOfRemCreaditLimit;
    }
    
    /*
       public Decimal getSumOfRemCreaditLimit(boolean isBottomLevelAccount,String currAcctId)
    {
        Decimal sumOfRemCreaditLimit=0.00;
        List<Account>  acctemp=null;
       // try{
             while (!isBottomLevelAccount) {
            		String qry='';
        			qry = ' Select id,parentId,akritiv__Account_Key__c,akritiv__Credit_Remaining__c from account where parentId=: currAcctId limit 1 ';
         			acctemp =Database.query(qry);
                 
                 if(acctemp != null && acctemp.size() > 0) {    
                     if (acctemp != null && acctemp.get(0).id != null) {
                    	currAcctId = acctemp.get(0).id;
                         System.debug('acctemp.parentId....'+acctemp.get(0).parentId);
                         if(acctemp.get(0).akritiv__Credit_Remaining__c != null)
                         	sumOfRemCreaditLimit += acctemp.get(0).akritiv__Credit_Remaining__c;
                         System.debug('sumOfRemCreaditLimit id is ....'+sumOfRemCreaditLimit);
                     }
                     else{
                         isBottomLevelAccount =true;
                     }
                 }
                 else{
                     isBottomLevelAccount =true;
                 }
             } 
        // } catch(Exception e){
                //system.debug('Error in getSumOfRemCreaditLimit ....'+e.getStackTraceString());  
       // } 
        return sumOfRemCreaditLimit;
    }*/
   
    global void finish(Database.BatchableContext BC) {
      // Todos: update final records 
    }
    
    // This method invoke schedualbleContext. The batch return in this method will be called automatically.  
    // Return Type - Void 
    // Parameter - sc SchedulableContext
    global void execute(SchedulableContext sc) {
    
        //get max batch num and it's source system
        try {

            BatchCalcTotRemainingCreditLimit bafl = new BatchCalcTotRemainingCreditLimit();
            Database.executeBatch(bafl, 200);

        } catch( Exception e ) {
            
        }


    }
}