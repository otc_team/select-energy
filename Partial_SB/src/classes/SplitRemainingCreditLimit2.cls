/*
 * Copyright (c) 2009-2018 Genpact Ltd. All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Genpact Ltd.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Genpact Ltd.
 *
 *
 * Usage :  This class will act as a controller for the Select Energy : Adjust Remaining Credit Limit. This class is having capability to Adjust the Total 
            Remaining Credit Limit of Ultimate Account to its Child Account.
 *
 */

public with sharing class SplitRemainingCreditLimit2 {
    
    public String accountId {get; set;}
    public String Location {get;set;}
    public Account account = null;
    
    // display tables
    public boolean isUltimateParentAvailableToDisplay{get;set;} 
    public boolean isParentAvailableToDisplay{get;set;} 
    public boolean isChildAvailableToDisplay{get;set;} 
    
    public List<Account> ultimateParentAccountList {get;set;}
    public List<Account> parentAccountList {get;set;}
    public List<Account> childAccountList {get;set;}
    
    // use of generic wrapper to avoid to use new field to store the split amount
    public List<SplitRemainingCreditLimitWrapper> listSplitWrapper{get;set;}
    
    
    // Default constructor
    public SplitRemainingCreditLimit2(){}
    
    // Param : ApexPages.StandardController - Standard Object
    public SplitRemainingCreditLimit2(ApexPages.StandardController controller){
        
        // get the Account id from parameter
        accountId = ApexPages.currentPage().getParameters().get('id');
        Location =  ApexPages.currentPage().getParameters().get('location');
        
        system.debug('STARK - Selected Account ID - '+ accountId );
        
        if( accountId != null){
                
            account = ( Account )controller.getRecord();
            system.debug('STARK - Selected Account - '+ account);
            
            // initialize with false
            isUltimateParentAvailableToDisplay = false;
            isParentAvailableToDisplay = false;
            isChildAvailableToDisplay = false;
            
            // initialize memory to Split Wrapper
            SplitRemainingCreditLimitWrapper splitWrapper = null; 
            listSplitWrapper = new List<SplitRemainingCreditLimitWrapper>();
            
            childAccountList = new List<Account>();
            
            // get the required fields from Account
            childAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,AccountSource,Total_Remaining_Credit_Limit__c, 
            						CreatedDate,Adjusted_Credit_Limit__c,akritiv__Source__c,Company_Code__c, 
            						akritiv__Credit_Limit__c, akritiv__Account_Key__c, akritiv__Total_AR__c, akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
            FROM Account where id =: accountId  ];
            
            system.debug('STARK - childAccountList - '+ childAccountList); // GROUP BY ParentId
            system.debug('STARK - child - Is current account is ultimate parent - ' + childAccountList[0].Is_Ultimate_Parent__c);
            system.debug('STARK - child - Is current account has parent - ' + childAccountList[0].ParentId);
            
            if( childAccountList[0].Is_Ultimate_Parent__c == true){
                
                system.debug('STARK - Section 1 - child account is ultimate parent. ');
                
                // The current Account is Ultimate Parent Account.
                isUltimateParentAvailableToDisplay = true;
                
                // As the current Account is ultimate parent then assign child account list to ultimate parent
                ultimateParentAccountList = childAccountList;
                
                parentAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,AccountSource,Total_Remaining_Credit_Limit__c,
                                        CreatedDate,Adjusted_Credit_Limit__c,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,
                                        akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
                                        FROM Account where ParentId =: childAccountList[0].Id  ];
                
                if( parentAccountList != null && parentAccountList.size() > 0 ){
                
                    isParentAvailableToDisplay = true;
                    
                    for( Account parentAcc : parentAccountList ){
            
                        splitWrapper = new SplitRemainingCreditLimitWrapper(); 
                        splitWrapper.account = parentAcc;
                        // splitWrapper.amount = 0;
                        listSplitWrapper.add(splitWrapper);
                    }
                    system.debug('STARK - listSplitWrapper - ' + listSplitWrapper );
                }
            } else {

                system.debug('STARK - Section 2 - child account is not ultimate parent. ');
                // current Account is not ultimate Parent
                
                if( childAccountList[0].ParentId != null){
                    
                    system.debug('STARK - Section 3 - child account has ultimate parent. ');
                    
                    parentAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,AccountSource,Total_Remaining_Credit_Limit__c,CreatedDate,
                                            Adjusted_Credit_Limit__c, akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,
                                            akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
                                            FROM Account where id =: childAccountList[0].ParentId  ];
                    
                    system.debug('STARK - parentAccountList - '+ parentAccountList); // GROUP BY ParentId
                    system.debug('STARK - parentAccountList[0].Is_Ultimate_Parent__c - ' + parentAccountList[0].Is_Ultimate_Parent__c);
                    system.debug('STARK - parentAccountList[0].ParentId- ' + parentAccountList[0].ParentId);
        
                    if( parentAccountList[0].ParentId != null ){
                        
                        system.debug('STARK - Section 4 ');
                        
                        // it has a Ultimate Parent Account
                        ultimateParentAccountList = [ SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,AccountSource,Total_Remaining_Credit_Limit__c,
                                                        CreatedDate,Adjusted_Credit_Limit__c,akritiv__Source__c,Company_Code__c,
                                                        akritiv__Credit_Limit__c,akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,
                                                        akritiv__Credit_Remaining__c 
                                                        FROM Account where id =: parentAccountList[0].ParentId  ];
                        
                        system.debug('STARK - ultimateParentAccountList - '+ ultimateParentAccountList); // GROUP BY ParentId
                        system.debug('STARK - ultimateParentAccountList[0].Is_Ultimate_Parent__c - ' + ultimateParentAccountList[0].Is_Ultimate_Parent__c);
                        system.debug('STARK - ultimateParentAccountList[0].ParentId- ' + ultimateParentAccountList[0].ParentId);
                        
                        if( ultimateParentAccountList[0].Is_Ultimate_Parent__c != true){
                            system.debug('STARK - Section 5 ');
                            system.debug('Ultimate Parent Account not found.');
                            isUltimateParentAvailableToDisplay = false;
                        
                        } else{
                        
                            system.debug('STARK - Section 6 ');
                            isUltimateParentAvailableToDisplay = true;
                            isParentAvailableToDisplay = true;
                            // isChildAvailableToDisplay = true;
                            
                            List<Account> allParentAccountList = [ SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,AccountSource,
                                                                    Total_Remaining_Credit_Limit__c,CreatedDate,Adjusted_Credit_Limit__c,
                                                                    akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,akritiv__Account_Key__c,
                                                                    akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
                                                                    FROM Account where ParentId =: parentAccountList[0].ParentId  ];
                            
                            system.debug('STARK - All parent account list - ' + allParentAccountList );
                            parentAccountList.clear();
                            system.debug('STARK - before parentAccountList - ' + parentAccountList );
                            parentAccountList = allParentAccountList;
                            system.debug('STARK - after parentAccountList - ' + parentAccountList );
                            
                            // parent list to wrapper list
                            for( Account parentAcc : parentAccountList ){
                    
                                splitWrapper = new SplitRemainingCreditLimitWrapper(); 
                                splitWrapper.account = parentAcc;
                                // splitWrapper.amount = 0;
                                listSplitWrapper.add(splitWrapper);
                            }
                            system.debug('STARK - listSplitWrapper - ' + listSplitWrapper );
                        }
                        
                    } else{
                        // this is Ultimate Parent Account
                        system.debug('STARK - Section 7 ');
                        ultimateParentAccountList = parentAccountList;
                        
                        parentAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,AccountSource,Total_Remaining_Credit_Limit__c,
                                                CreatedDate,Adjusted_Credit_Limit__c, akritiv__Source__c,Company_Code__c,
                                                akritiv__Credit_Limit__c,akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,
                                                akritiv__Credit_Remaining__c 
                                                FROM Account where ParentId =: ultimateParentAccountList[0].Id  ];
                
                        isUltimateParentAvailableToDisplay = true;
                        if( parentAccountList != null && parentAccountList.size() > 0 ){
                            
                            system.debug('STARK - Section 8 ');
                            
                            isParentAvailableToDisplay = true;
                            
                            for( Account parentAcc : parentAccountList ){
                    
                                splitWrapper = new SplitRemainingCreditLimitWrapper(); 
                                splitWrapper.account = parentAcc;
                                // splitWrapper.amount = 0;
                                listSplitWrapper.add(splitWrapper);
                            }
                            system.debug('STARK - listSplitWrapper - ' + listSplitWrapper );
                        }
                    }
                } else{
                    system.debug('STARK - Section 9 ');
                    isUltimateParentAvailableToDisplay = false;
                    system.debug('STARK - No Ultimate Account found');
                }
            }
            
        } else {
            system.debug('STARK - Section 10 ');
            system.debug('Account Id is null.');
        }
    }

    public PageReference saveNewCreditLimitValues(){
    
        system.debug('STARK - save - listSplitWrapper - ' + listSplitWrapper);
        
        Account acc = null;
        List<Account> listToUpdate = new List<Account>();
        
        for( SplitRemainingCreditLimitWrapper splitWrapObj : listSplitWrapper ){
            
            if(splitWrapObj.selected){
	            system.debug('STARK - splitWrapObj - ' + splitWrapObj);
	            acc = new Account();
	            splitWrapObj.account.Adjusted_Credit_Limit__c = splitWrapObj.amount;
	            acc = splitWrapObj.account;
	            listToUpdate.add(acc);
            }
            
        }
        system.debug('STARK - updated listToUpdate - ' + listToUpdate);
        update listToUpdate;
        return null;
        // return new Pagereference('/'+ account.Id );
    }
    
}