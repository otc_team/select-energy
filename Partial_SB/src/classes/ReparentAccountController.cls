/*
 * Copyright (c) 2010-2011 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
 
/*
 * Usage: This Class is Used to Reparent Account of selected Transaction from the Transaction Detail Page
 *        It Transferred the related Disputes on Account of selected Transaction and It also transferred
 *        the related Tasks , Notes and Attachments on Selected Transactions.         
 */
 
public with sharing class ReparentAccountController { 
    public String location { get; set; }
    public String key { get; set; }
    public Account acc { get; set; }
    public Boolean isClose { get; set; }
    //public Boolean isBtndisabled { get; set; }
    public List<Account> accList { get; set; }     
    public String SelectedAcct{get;set;}
       
    public ReparentAccountController(){         
        acc = new Account();      
        key =  ApexPages.currentPage().getparameters().get('key');
        location =  ApexPages.currentPage().getparameters().get('location');
        key = key.trim();
        location = location.trim();  
        isClose = true;  
        //isBtndisabled = true;
    }
    
    public PageReference SearchAccount() {       
        accList = [ SELECT Id,Name,akritiv__DPD__c,AccountNumber,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Account_Key__c FROM Account WHERE Name =: acc.Name ];        
        if ( accList.size() == 0 ){
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Account Not Found , Please Enter Valid Account Name.')); 
            //isBtndisabled = true;   
        }
        else{
            //isBtndisabled = false;            
        }                                  
        return null;
    }      
                      
    public PageReference ReparentAccount() {       
        //system.debug('----SelectedAcct---->>'+SelectedAcct);
        akritiv__Temp_Object_Holder__c temp = [ SELECT Id, akritiv__Value__c from akritiv__Temp_Object_Holder__c WHERE akritiv__key__c =: key];                                                
        String txStr = temp.akritiv__Value__c;
        String  [] txStrArray = txStr.split(',');    
        List<Account> tempAcc1 = new List<Account>();
        Account tempAcc = new Account();
        
        if ( SelectedAcct != '' ||  SelectedAcct != null ){
           tempAcc1 = [SELECT Id FROM Account WHERE  Id =: SelectedAcct] ;                        
           if ( tempAcc1.size() == 0 ){
               Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Please search the valid Account then press Re Parent button!'));
               isClose = false;
               return null;
           }
           else{
               tempAcc = tempAcc1.get(0);
               isClose = true;
          }
        }
                        
        Map<String,String> txIdKeyMap = new Map<String,String>();

        String soqlTx = getCreatableFieldsSOQL('akritiv__transaction__c','Id IN : txStrArray');
        List<akritiv__transaction__c> oldTxList = Database.query(soqlTx);
        List<akritiv__transaction__c> newTxToInsert = new List<akritiv__transaction__c>();

        for(akritiv__transaction__c tx : oldTxList){
            txIdKeyMap.put(tx.Id,tx.akritiv__transaction_key__c);
            akritiv__transaction__c tx1 = tx.clone(false,true);
            tx1.akritiv__Account__c = tempAcc.Id;
            newTxToInsert.add(tx1);
        }
        
        //Task Update
        String soqlTask = getCreatableFieldsSOQL('Task','WhatId IN : txStrArray');                        
        List<Task> oldTaskList = Database.query(soqlTask);        
        List<Task> newTaskList = new List<Task>();
        Map<String,List<Task> > newTaskWhatMap = new  Map<String,List<Task> >();
        List<Task> tempTaskList;
        
        for(Task tsk : oldTaskList){
            Task  tsk1 = tsk.clone(false,true);
            if (newTaskWhatMap.get(txIdKeyMap.get(tsk.WhatId))!= NULL){
                tempTaskList = new List<Task>();
                tempTaskList = newTaskWhatMap.get(txIdKeyMap.get(tsk.WhatId));
                tempTaskList.add(tsk1);
                newTaskWhatMap.put(txIdKeyMap.get(tsk.WhatId),tempTaskList);
            }else{
                tempTaskList = new List<Task>();
                tempTaskList.add(tsk1);
                newTaskWhatMap.put(txIdKeyMap.get(tsk.WhatId),tempTaskList);
            } 
        } 
        
        //Note Update
        String soqlNoteOnTx = getCreatableFieldsSOQL('Note','ParentId IN : txStrArray');                        
        List<Note> oldNoteOnTxList = Database.query(soqlNoteOnTx);        
        List<Note> tempNote = new List<Note>();
        List<Note> newNoteOnTxList = new List<Note>();
        
        Map<String,List<Note>> newNoteWhatMap = new Map<String,List<Note>>();         
        
        for(Note noteObj : oldNoteOnTxList){
            Note note1 = noteObj.clone(false,true);
            if (newNoteWhatMap.get(txIdKeyMap.get(noteObj.parentId))!= NULL){
                tempNote = new List<Note>();
                tempNote = newNoteWhatMap.get(txIdKeyMap.get(noteObj.parentId));
                tempNote.add(note1);
                newNoteWhatMap.put(txIdKeyMap.get(noteObj.parentId),tempNote);
            }else{
                tempNote = new List<Note>();
                tempNote.add(note1);
                newNoteWhatMap.put(txIdKeyMap.get(noteObj.parentId),tempNote);
            }             
        } 
        
        // Attachment Update
        String soqlAttachOnTx = getCreatableFieldsSOQL('Attachment','ParentId IN : txStrArray');                        
        List<Attachment> oldAttachOnTxList = Database.query(soqlAttachOnTx);        
        List<Attachment> newAttachOnTxToInsert = new List<Attachment>();
        List<Attachment> tempAttach = new List<Attachment>();
        Map<String,List<Attachment>> attachOnTxWhatMap = new Map<String,List<Attachment>>(); 
        
        for(Attachment attachTxObj : oldAttachOnTxList){
            Attachment attach1 = attachTxObj.clone(false,true);
            if (attachOnTxWhatMap.get(txIdKeyMap.get(attachTxObj.parentId))!= NULL){
                tempAttach = new List<Attachment>();
                tempAttach = attachOnTxWhatMap.get(txIdKeyMap.get(attachTxObj.parentId));
                tempAttach.add(attach1);
                attachOnTxWhatMap.put(txIdKeyMap.get(attachTxObj.parentId),tempAttach);
            }else{
                tempAttach = new List<Attachment>();
                tempAttach.add(attach1);
                attachOnTxWhatMap.put(txIdKeyMap.get(attachTxObj.parentId),tempAttach);
            } 
        }
               
       
        //Dispute Update
        String soqlDisp = getCreatableFieldsSOQL('akritiv__Dispute__c','akritiv__Transaction__c IN : txStrArray');
        List<akritiv__Dispute__c> dispList = Database.query(soqlDisp);
        
        List<akritiv__Dispute__c> disptoInsert = new List<akritiv__Dispute__c>();
        List<akritiv__Dispute__c> dispToDelete= new List<akritiv__Dispute__c>();

        DELETE oldTxList;
        //system.debug('====del========oldTxList===========>>'+oldTxList);
        List<akritiv__Transaction__c> testTxList = new List<akritiv__Transaction__c>();
        
        if ( newTxToInsert.size() > 0){  
            for ( akritiv__Transaction__c tx : newTxToInsert ){
                tx.akritiv__Disputed_Amount__c = 0;  
                testTxList.add(tx);         
            }
        }
        
        if(testTxList.size() > 0){        
            INSERT testTxList;
            //system.debug('=====ins=======testTxList===========>>'+testTxList);
        }
        /*if(newTxToInsert.size() > 0){        
            INSERT newTxToInsert;
            system.debug('=====ins=======newTxToInsert===========>>'+newTxToInsert);
        }*/
        
        Map<String,Id> newTxIdKeyMap = new Map<String,Id>();
        for(akritiv__transaction__c tx : newTxToInsert){
            newTxIdKeyMap.put(tx.akritiv__transaction_key__c,tx.Id);
        }

        for(akritiv__Dispute__c disp : dispList){
            akritiv__Dispute__c disp1 = disp.clone(false,true);
            disp1.akritiv__Account__c = tempAcc.Id;
            disp1.akritiv__Transaction__c = newTxIdKeyMap.get(disp.akritiv__transaction__r.akritiv__Transaction_Key__c);
            disp.akritiv__Transaction__c = newTxIdKeyMap.get(disp.akritiv__transaction__r.akritiv__Transaction_Key__c);
            dispToDelete.add(disp);
            disptoInsert.add(disp1);
        }       
        
        for(akritiv__transaction__c tx : newTxToInsert){
           
            if(newTaskWhatMap.get(tx.akritiv__Transaction_Key__c) != null){
                tempTaskList  = newTaskWhatMap.get(tx.akritiv__Transaction_Key__c);
                for ( Task tskObj : tempTaskList){
                    tskObj.whatId = newTxIdKeyMap.get(tx.akritiv__Transaction_Key__c);
                    newTaskList.add(tskObj);
                }
            }
            
            if ( newNoteWhatMap.get(tx.akritiv__Transaction_Key__c) != null){
                tempNote = newNoteWhatMap.get(tx.akritiv__Transaction_Key__c);
                for ( Note tNote : tempNote){
                    tNote.ParentId = newTxIdKeyMap.get(tx.akritiv__Transaction_Key__c);
                    newNoteOnTxList.add(tNote);
                }
            }
            
           if ( attachOnTxWhatMap.get(tx.akritiv__Transaction_Key__c) != null){
                tempAttach = attachOnTxWhatMap.get(tx.akritiv__Transaction_Key__c);
                for ( Attachment tAttach : tempAttach){
                    tAttach.ParentId = newTxIdKeyMap.get(tx.akritiv__Transaction_Key__c);
                    newAttachOnTxToInsert.add(tAttach);
                }
            }
        }                 
                
        INSERT newTaskList;
        INSERT newNoteOnTxList;
        INSERT newAttachOnTxToInsert;
                                        
        if(dispToDelete.size()>0){
            DELETE dispToDelete;
            //system.debug('=====del=======dispToDelete===========>>'+dispToDelete);
        }
        if(disptoInsert.size() > 0){    
            INSERT disptoInsert;
            //system.debug('====ins========disptoInsert===========>>'+disptoInsert);
        }  
        
        return null;                  
     } 
    
     public static string getCreatableFieldsSOQL(String objectName, String whereClause){
 
        String selects = '';
 
        if (whereClause == null || whereClause == ''){ return null; }
 
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
 
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable                    
                    selectFields.add(fd.getName());
                }
            }
        }
 
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
 
        }
        if(objectName == 'akritiv__Dispute__c' ){
              return 'SELECT akritiv__transaction__r.akritiv__Transaction_Key__c,' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
        }else{
            return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
        }
 
    }        
        
    // TEST CLASS for the ReparentController
    
    static testMethod void testCase1()
    {
        String txIds = '';
        Account aAccount = new Account();
        aAccount.name = 'A Account';
        aAccount.akritiv__account_key__c ='AAccountKey';
               
        try{
            INSERT aAccount;
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        
        List<akritiv__Transaction__c> txList = new List<akritiv__Transaction__c>();
        
        akritiv__Transaction__c tx1 = new akritiv__Transaction__c();
        tx1.name = 'tx1';
        tx1.akritiv__Transaction_Key__c = 'key-tx1';
        tx1.akritiv__Account__c = aAccount.Id;
        tx1.akritiv__Amount__c = 10.10;
        tx1.akritiv__Balance__c = 20.20;
        
        try{
            INSERT tx1;
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        
        Task tsk1 = new Task();
        tsk1.Subject = 'tsk1';
        tsk1.WhatId = tx1.Id;
        try{
            INSERT tsk1;
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        
        Note note1 = new Note();
        note1.Title = 'note1';
        note1.ParentId = tx1.Id;
        try{
            INSERT note1;
        }catch(Exception e){
            system.debug(e.getMessage());
        }        
        
        Attachment attach1 = new Attachment();
        attach1.Body = Blob.valueOf('this is testing');
        attach1.Name = 'attach1';
        attach1.ParentId = tx1.Id;
        try{
            INSERT attach1;
        }catch(Exception e){
            system.debug(e.getMessage());
        }        
        
        txList.add(tx1);
        
        
        for(akritiv__Transaction__c tx : txList)
        {
            txIds += ','+tx.Id;
        }
        akritiv__Temp_Object_Holder__c Config = new akritiv__Temp_Object_Holder__c();
        Config.akritiv__key__c = Userinfo.getUserName()+Datetime.now();
        Config.akritiv__value__c = txIds;
        try{
            INSERT Config;
        }catch(Exception e){
            system.debug(e.getMessage());
        }       
        
        ApexPages.currentPage().getParameters().put('key',Config.akritiv__key__c);
        ApexPages.currentPage().getParameters().put('location','https://cs11.salesforce.com/');
                     
        ReparentAccountController rac = new ReparentAccountController();
        
        rac.acc.akritiv__Account_Key__c = 'AAccountKey';        
        rac.acc.name ='A Account';
        rac.SelectedAcct = aAccount.id;
        rac.SearchAccount();        
        rac.ReparentAccount();
        
        ReparentAccountController rac1 = new ReparentAccountController();
        rac1.acc.akritiv__Account_Key__c = 'WrongKey';  
        rac1.acc.name ='B Account';  
                
        rac1.SearchAccount();        
        rac1.ReparentAccount();           
    }
}