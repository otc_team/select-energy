/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This Batch class is used for Closing batch processes automatically.
 */
global class BatchCreateUltimateAccount implements Database.Batchable<SObject>, Database.Stateful, Schedulable  {
    
    public Exception  ex;
    public Long timesInMillis = System.currentTimeMillis();
    
    global Map< String, Account > existingParentAccount = new Map< String, Account>();
    global Map< String, Account > existingultimateAccount = new Map< String, Account>();
    
    // Global Default Constructor
    global BatchCreateUltimateAccount(){
        // ToDos: 
    }

    // start method implementation of Bachable to read the Child Accounts
    // Parameters - BC Database.Batchablecontext
    global Database.Querylocator start(Database.BatchableContext BC) {
      
      String query = '';
        try{
            
          String capacity = '1';
            
            String Source = 'TEST';
            Boolean isUltip = false;
            
            query = 'Select Id, Name, AccountNumber, akritiv__Account_Key__c, Parent_Company_No__c, ParentId, Is_Parent__c, ';
            query += ' akritiv__Source__c, akritiv__Batch_Number__c, Is_Ultimate_Parent__c from Account ';
            query += ' Where Is_Parent__c = true and Parent_Company_No__c != null and ParentId = null and Is_Ultimate_Parent__c =: isUltip';
            query += ' and New_Parent_Company_No__c =null order by AccountNumber';
         
        //  
         // and akritiv__Source__c=:Source
         } catch(Exception e){
                  // sendMail();
        }
        
        system.debug('---Query--->> '+query);
        return Database.getQueryLocator(query);
        
    }
       
    // Method to create the Ultimate / Super Account record.
    // Return Type - Void 
    // Parameters - BC Database.Batchablecontext , List<SObject> scope
    global void execute(Database.BatchableContext BC, List<Sobject> sObjs) {
    
    	existingparentAccount.clear();//ok
        existingultimateAccount.clear();//ok
    
    	Boolean isupdate = false;

        List<Account> accountsToCreate = new List<Account>();
        Map<ID,Account> updatemapwrap= new Map<ID,Account>();
         
        Set<String> parentbinSet = new Set<String>();
        Set<String> ultbinSet = new Set<String>();
         
        try{
        
        	for(Sobject accSobject : sObjs) {
            	Account act = (Account) accSobject;
              
              	if ( act.Parent_Company_No__c != null ){

                	parentbinSet.add(act.Parent_Company_No__c);
              	}
			}
          	
          	system.debug('----> parentbinSet :'+ parentbinSet);     
  
  			List<Account> accountToBeUpsert = new List<Account>();
		  	List<Account> accountinsert = new List<Account>();
		  	List<Account> notoinssert = new List<Account>();
  
          	Map<String ,List <Account > > accountParentMap = new Map<String ,List <Account >>();
          	Map< String, Account > ChildParentAccount = new Map< String, Account>();
          
          	List<Account> parentBINACCONTS = [select Id, Name, akritiv__Account_Key__c, ParentId, AccountNumber, Parent_Company_No__c 
                                    				from Account where Parent_Company_No__c in : parentbinSet];
      
	      	system.debug('----> parentBINACCONTS :'+ parentBINACCONTS);
	      	system.debug('----> parentBINACCONTS Size :'+ parentBINACCONTS.size());
      
          	for ( Account a : parentBINACCONTS ){
  
            	existingparentAccount.put(a.Parent_Company_No__c,a);
              
          	}
         
          	system.debug('----> existingParentAccount :'+ existingparentAccount);
          
          	/*  1. Identify & Create the Ultimate Account
                2. Assignment of Ultimate Account it's child Account. 
          	*/ 
          	for(Sobject accSobject:sObjs) {
            
	            Account account = (Account) accSobject;
	            
	            system.debug('----> START existingparentAccount :'+ existingparentAccount);
	            system.debug('----> account :'+ account);
	            system.debug('----> account.AccountNumber :'+ account.AccountNumber);
	            system.debug('----> account.Key :'+ account.akritiv__Account_key__c);
	            system.debug('----> existingparentAccount.containskey (Account # ) :'+ existingparentAccount.containskey( account.AccountNumber));
	            system.debug('----> existingparentAccount.containskey (Account KEY ) :'+ existingparentAccount.containskey( account.akritiv__Account_key__c));
	            
	            if( existingparentAccount.containskey(account.AccountNumber)){
              
              		// Check if Account is exist with same Key
              		Account headquateraccount = existingparentAccount.get( account.AccountNumber );
              
              		system.debug('----> headquateraccount :'+ headquateraccount);
              
              		if ( headquateraccount == null ) {

                       // headquateraccount = existingultimateAccount.get( account.AccountNumber );
                        
                    }
              
              		system.debug('----> headquateraccount.id :'+ headquateraccount.id);              
              
              		if ( headquateraccount.Id == null ) {
						
						if ( !accountParentMap.containsKey( headquateraccount.akritiv__Account_key__c )) {

                            List < Account > lst = new List < Account > ();
                            lst.add( account ) ;
                            accountParentMap.put( headquateraccount.akritiv__Account_key__c ,lst );
              
              				system.debug('----> accountParentMap.Put :'+ accountParentMap);

                        } else {

                            accountParentMap.get( headquateraccount.akritiv__Account_key__c ).add( account );
                            system.debug('----> accountParentMap.get :'+ accountParentMap);
                        }
                    } else {
                      
			            system.debug('----> headquateraccount.Id :'+ headquateraccount.Id);
			            account.parentId = headquateraccount.Id;
                                               
                    }
          
          
          			system.debug('----> accountToBeUpsert account :'+ account);
                    accountToBeUpsert.add( account);
              
            } else{
				// todos: Create ultimate account
				system.debug('----> Create ultimate account : '+ account);
				//create new
                Account newAcc = new Account();
                newAcc.Name = account.Name + ' ['+'Ultimate Parent]';
                newAcc.AccountNumber = account.AccountNumber;
                //newAcc.Is_Parent__c = account.Is_Parent__c;
                newAcc.akritiv__Account_key__c = account.AccountNumber;
                newAcc.akritiv__Source__c = account.akritiv__Source__c;
                newAcc.akritiv__Batch_Number__c = account.akritiv__Source__c + DateTime.now().format('ddMMyyyyhhmmss');
                newAcc.Is_Ultimate_Parent__c = true;

          		system.debug('----> accountParentMap : '+ accountParentMap);
          
                if ( !accountParentMap.containsKey( newAcc.akritiv__Account_key__c )) {

                    List < Account > lst = new List < Account > ();
                    lst.add( account ) ;
                    accountParentMap.put( newAcc.akritiv__Account_key__c ,lst );
        
        			system.debug('----> New accountParentMap.Put :'+ accountParentMap);

                } else {

                    accountParentMap.get( newAcc.akritiv__Account_key__c ).add( account );
                    system.debug('----> New accountParentMap.get :'+ accountParentMap);
                }

          		system.debug('----> Upsert account :'+ account);
          		system.debug('----> New newAcc :'+ newAcc);
          
                accountToBeUpsert.add(account);
                accountinsert.add(newAcc);
                existingparentAccount.put(account.AccountNumber,newAcc);
                ChildParentAccount.put(newAcc.akritiv__Account_key__c,account);
                system.debug('----> END existingparentAccount :'+ existingparentAccount);
                    
              
        	}
            
		}
          
	    system.debug('---->accountinsert : '+ accountinsert);
	    system.debug('---->accountToBeUpsert : '+ accountToBeUpsert);
          
        if ( accountinsert.size() > 0 ) {

	    	insert accountinsert;
	      	system.debug('---->accountinsert : '+ accountinsert);
        }
           
        List <Account> updateforParentUT = new List<Account>();
        
        for(Account insacc:accountinsert){
             if (accountParentMap.containsKey(insacc.akritiv__Account_key__c) ){
                    List<Account> accPrnt = accountParentMap.get(insacc.akritiv__Account_key__c);
                    for(Account Acclst :accPrnt){
                    Account ac = new Account(id = acclst.id);
                    ac.ParentID = insacc.Id;
                    updateforParentUT.add(ac);
                    }
             }                      
          }
          
          if ( accountToBeUpsert.size() > 0 ) {

              update accountToBeUpsert;
              system.debug('---->accountToBeUpsert : '+ accountToBeUpsert);
          }
          
          if ( updateforParentUT.size() > 0 ) {

              update updateforParentUT;
              system.debug('---->accountToBeUpsert : '+ accountToBeUpsert);
          }
          
          // Update Child Account Credit limit to the Parent Account.  
          
          
          
          
        }catch(Exception e){
                 system.debug('-----Exception----> '+ e);
        }
   
    }
    
    
    global void finish(Database.BatchableContext BC) {
      // Todos: update final records 
    }
    
    // This method invoke schedualbleContext. The batch return in this method will be called automatically.  
    // Return Type - Void 
    // Parameter - sc SchedulableContext
    global void execute(SchedulableContext sc) {
    
        //get max batch num and it's source system
        try {

            BatchCreateUltimateAccount bafl = new BatchCreateUltimateAccount();
            Database.executeBatch(bafl, 200);

        } catch( Exception e ) {
            
        }
    }
}