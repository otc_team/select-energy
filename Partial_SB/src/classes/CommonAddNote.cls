public with sharing class CommonAddNote{

    public String commonNote { get; set; }
    public String location { get; set; }
    public String PageId { get; set; }
    
    public List<akritiv__Dispute__c> dips;
    public List<akritiv__Transaction__c> trxs;
    public List<Account> acs;
    
    
    public CommonAddNote(){
        try {
        
            String dId = ApexPages.currentPage().getParameters().get('dsId');
            String TrxId = ApexPages.currentPage().getParameters().get('trxId');
            PageId = ApexPages.currentPage().getParameters().get('page');
            
            location  = ApexPages.currentPage().getParameters().get('location');
            
            if(dId != null && dId != '' && PageId == 'dispute'){
                dips = new List<akritiv__Dispute__c>([select id, Name , Note__c,LastModifiedDate, akritiv__Account__c from akritiv__Dispute__c where id =: dId limit 1]);
                
                if(dips.size() > 0 ){
                
                    acs = new List< Account >([ select Id , Name , LastModifiedDate, akritiv__account_Key__c, Owner.Name from Account where Id =: dips.get(0).akritiv__Account__c limit 1]);
                    
                   // commonNote = dips.get(0).LastModifiedDate + ' ' + userInfo.getName() + ' :';
                }
            
            }else if(TrxId != null && TrxId != '' && PageId == 'Transaction'){
                trxs= new List<akritiv__Transaction__c>([select id, Name , Note__c,LastModifiedDate, akritiv__Account__c from akritiv__Transaction__c where id =: TrxId limit 1]);
                
                if(trxs.size() > 0 ){
                
                    acs = new List< Account >([ select Id , Name , LastModifiedDate, akritiv__account_Key__c, Owner.Name from Account where Id =: trxs.get(0).akritiv__Account__c limit 1]);
                    
                   // commonNote = trxs.get(0).LastModifiedDate + ' ' + userInfo.getName() + ' :';
                }
                
            }
        
        }catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    
    public PageReference cancel() {
        return null;
    }


    public PageReference savenote() {
        
        if(PageId == 'dispute'){
            akritiv__Dispute__c dp = new akritiv__Dispute__c();
            if(dips.size() > 0){
                dp = dips.get(0);
           
                if(dp.Note__c == null)
                    dp.Note__c = '';
                    String cNote = dp.LastModifiedDate + ' ' + userInfo.getName() + ' : ';
                    dp.Note__c = cNote + commonNote + '\n'+ dp.Note__c;
                
                update dp;    
            }
        }
        
        if(PageId == 'Transaction'){
            akritiv__Transaction__c tr = new akritiv__Transaction__c();
            if(trxs.size() > 0){
                tr = trxs.get(0);
           
                if(tr.Note__c == null)
                    tr.Note__c = '';
                    String cNote = tr.LastModifiedDate + ' ' + userInfo.getName() + ' : ';
                    tr.Note__c = cNote + commonNote + '\n'+ tr.Note__c;
                
                update tr;    
            }
        }
        
        return null;
    }
    
    static testmethod void test(){
    List<akritiv__Dispute__c> disList = [select id,name from akritiv__Dispute__c limit 1];
 
    ApexPages.currentPage().getParameters().put('dsId',disList.get(0).Id);
   
    ApexPages.currentPage().getParameters().put('page','dispute');
    CommonAddNote common = new CommonAddNote();
    common.cancel();
    common.saveNote();
    }
     static testmethod void test1(){
   
    List<akritiv__Transaction__c> txList = [select id,name from akritiv__Transaction__c limit 1];
  
    ApexPages.currentPage().getParameters().put('trxId',txList.get(0).Id);
    ApexPages.currentPage().getParameters().put('page','transaction');
    CommonAddNote common = new CommonAddNote();
    common.cancel();
    common.saveNote();
    }
    static testmethod void test2(){
   
        List<Account> accList = [select id,name from Account limit 1];
        akritiv__Dispute__c akd = new akritiv__Dispute__c();
        akd.akritiv__Account__c = accList.get(0).Id;
        akd.note__c = null;
        insert akd;
        ApexPages.currentPage().getParameters().put('dsId',akd.Id);
       
        ApexPages.currentPage().getParameters().put('page','dispute');
        CommonAddNote common = new CommonAddNote();
        common.cancel();
        common.saveNote();
   
    }
    static testmethod void test3(){
   
        List<Account> accList = [select id,name from Account limit 1];
        akritiv__Transaction__c akd = new akritiv__Transaction__c();
        akd.akritiv__Account__c = accList.get(0).Id;
        akd.akritiv__Amount__c = 100;
        akd.akritiv__Balance__c =100;
        akd.note__c = null;
        insert akd;
        ApexPages.currentPage().getParameters().put('trxId',akd.Id);
       
        ApexPages.currentPage().getParameters().put('page','transaction');
        CommonAddNote common = new CommonAddNote();
        common.cancel();
        common.saveNote();
   
    }
}