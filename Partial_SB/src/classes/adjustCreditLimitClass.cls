/*
 * Copyright (c) 2009-2018 Genpact Ltd. All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Genpact Ltd.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Genpact Ltd.
 *
 *
 * Usage :  This class will act as a controller for the Select Energy : Adjust Remaining Credit Limit. This class is having capability to Adjust the Total 
            Remaining Credit Limit of Ultimate Account to its Child Account.
 *
 */
public class adjustCreditLimitClass {
    
    public string accountId {get; set;}
    public string Location {get;set;}
    
    public list<accountWrapper> recordlist { get; set;}
    public list<selectoption> status;
    public string selectedstatus { get; set;}
    
    public Account selectedAccount {get;set;} 
    public Account ultimateAccount { get; set;}
    public list<Account> parentAccountList { get; set;}
    
    // Display tables
    public boolean isUltimateParentAvailableToDisplay{get;set;} 
    public boolean isParentAvailableToDisplay{get;set;} 
    
    // Default constructor
    public adjustCreditLimitClass(){}
     
    public adjustCreditLimitClass(ApexPages.StandardController controller){
    	
    	// get the Account id from parameter
        accountId = ApexPages.currentPage().getParameters().get('id');
        Location =  ApexPages.currentPage().getParameters().get('location');
    	
    	try{
    		
    		if(accountId != null){
    		
    			//isUltimateParentAvailableToDisplay = false;
            	//isParentAvailableToDisplay = false;
    		
	    		// Query: Get selected account's required fields details. 
	            selectedAccount = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,Parent.Id,Parent.Parent.Id,AccountNumber,Total_Remaining_Credit_Limit__c,
	            						CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,Is_Parent__c,
	            						akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
	            						FROM Account where id =: accountId limit 1 ];
	    		
		    	
		    	system.debug('----selectedAccount---->> '+ selectedAccount);
		    	
		    	if( selectedAccount.Is_Ultimate_Parent__c == true ){
		    		
		    		// ToDo : I'm Ultimate Account. The selected account is Ultimate Account
		    		/*
		    			NOTE: Query on Account to fetch all the child account of the selected Account. 
		    		*/
		    		ultimateAccount = new Account();
		    		
		    		ultimateAccount = selectedAccount;
		    		
		    		system.debug('----ultimateAccount---->> '+ selectedAccount);
		    		
		    		// The current Account is Ultimate Parent Account.
                	isUltimateParentAvailableToDisplay = true;
		    				    		
		    		parentAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,Total_Remaining_Credit_Limit__c,
	            						CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,Is_Parent__c,
	            						akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
	            						FROM Account
	            						where ParentId =: ultimateAccount.Id ];
		    		
		    	} else if(selectedAccount.Is_Parent__c == true && selectedAccount.ParentId != null && selectedAccount.Is_Ultimate_Parent__c == false ) {
		    		
		    		// ToDo : I'm Ultimate Parent's Child Account. The selected account is Ultimate Parent's Child Account.
		    		
		    		ultimateAccount = new Account();
		    		
		    		// The current Account is Ultimate Parent Account.
                	isUltimateParentAvailableToDisplay = true;
		    		
		    		//ultimateAccount = selectedAccount.Parent.ID;
		    		
		    		system.debug('----selectedAccount.ParentId---->> '+ selectedAccount.ParentId);
		    		
		    		ultimateAccount = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,Total_Remaining_Credit_Limit__c,
	            						CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,Is_Parent__c,
	            						akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
	            						FROM Account where id =: selectedAccount.ParentId limit 1 ];
	            						
	            						
	            	parentAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,Total_Remaining_Credit_Limit__c,
	            						CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,Is_Parent__c,
	            						akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
	            						FROM Account
	            						where ParentId =: ultimateAccount.Id ];
		    		
			        
		    	} else if (selectedAccount.Is_Parent__c == false && selectedAccount.ParentId != null && selectedAccount.Is_Ultimate_Parent__c == false ) {
		    	
		    		// ToDo : I'm Grand Child Account. The selected account is Grand Child Account.
		    	
		    		ultimateAccount = new Account();
		    		
		    		// The current Account is Ultimate Parent Account.
                	isUltimateParentAvailableToDisplay = true;
		    		
		    		//ultimateAccount = selectedAccount.Parent;
		    		system.debug('----selectedAccount.Parent.Parent---->> '+ selectedAccount.Parent.Parent.id);
		    		
		    		ultimateAccount = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,Total_Remaining_Credit_Limit__c,
	            						CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,Is_Parent__c,
	            						akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
	            						FROM Account where Id =: selectedAccount.Parent.Parent.id limit 1 ];
	            						
	            						
	            	parentAccountList = [SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,Total_Remaining_Credit_Limit__c,
	            						CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c,Is_Parent__c,
	            						akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c 
	            						FROM Account
	            						where ParentId =: ultimateAccount.Id ];
		    		
			        
		    	
		    	
		    	} else {
		    	
		    		// Todo: No Ultimate Account found for the selected account.
		    		
		    		isUltimateParentAvailableToDisplay = false;
		    		isParentAvailableToDisplay = false;
		    		system.debug('No Ultimate Account or Grand parent Account found.');
		    	
		    	}
		    	
		    	//Display Parent Records
		    	if(parentAccountList.size()!=0){
			        	
		            recordlist = new list<accountWrapper>();
		        			        
			        for(Account act:parentAccountList){
			            recordlist.add(new accountWrapper(act));
			        } 
			         
			        isParentAvailableToDisplay = true;
			        
		    	}else{
		    		
		    		isParentAvailableToDisplay = false;
		    	}
		    	
	
    		}
	
    	}Catch( Exception e){
    	
    		// Add exception: Account Id has been removed from URL.
    	
    	}
    	
             
	}
    
    /*
    	This method is used to update the Credit Limit value for the selected Account. 
    	Param : No Parameter.
    */ 
    public PageReference saveAdjustedCreditLimit(){
    	
        list<Account> actListUpdate = new list<Account>();
        
        for(accountWrapper pw: recordlist){
            if(pw.selected){
               
                System.debug('-----adjustCreditLimitAmount----->> '+ pw.adjustCreditLimitAmount);
                pw.record.Adjusted_Credit_Limit__c = pw.adjustCreditLimitAmount;
                pw.selected=false;
                
                
                actListUpdate.add(pw.record);
            }
        }
        
        if(actListUpdate.size() > 0){
        	update actListUpdate;
        }
       
        return null;
    }   
    
    // Account Wrapper Class  
    public class accountWrapper{
    	
        public boolean selected { get; set;}
        public Account record  { get; set;}
        public Integer numberOfEmployeesCount { get; set;}
        public Integer adjustCreditLimitAmount { get; set;}
        
        public accountWrapper(Account record){
            this.record = record;
            selected = false;
            numberOfEmployeesCount = 0;
            adjustCreditLimitAmount = 0;
        }
    }
    
    public Static List <Account> fetchRecords(String whereCriteria){
    	   
        String reversalQuery = 'SELECT Id, Name, Is_Ultimate_Parent__c, ParentId,AccountNumber,Total_Remaining_Credit_Limit__c, ';
	           reversalQuery += ' CreatedDate,akritiv__Source__c,Company_Code__c,akritiv__Credit_Limit__c, ';
	           reversalQuery += ' akritiv__Account_Key__c,akritiv__Total_AR__c,akritiv__Total_Past_Due__c,akritiv__Credit_Remaining__c ';
	           reversalQuery += ' FROM Account where '+ whereCriteria;
        
        
        return Database.query(reversalQuery);
    }
}