global class SplitRemainingCreditLimitWrapper {

    // Objects contained in wrapper
    public Account account {get;set;}
    public integer amount {get;set;}
    public boolean selected { get; set;}
    
    global SplitRemainingCreditLimitWrapper (){
        
        system.debug('SplitRemainingCreditLimitWrapper called.');
        selected = false;
        account = null;
        amount = 0;
    }
}