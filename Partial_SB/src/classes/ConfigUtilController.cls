/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Application wide utility controller for configuration.
 */
public with sharing class ConfigUtilController {
         
    // Fetches Default values specified in SysConfig_Singlec__C Custom Setting
    // Return Type - SysConfig_Singlec__C 
    // No parameters are passed in this method 
    public static akritiv__SysConfig_Singlec__c getSystemConfigObj() {
        if(akritiv__SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            return akritiv__SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(akritiv__SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            return akritiv__SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }

        return akritiv__SysConfig_Singlec__c.getOrgDefaults();
    }
   
   
   // Fetches the EmailIDs which is specified in BatchJobNotificationEmailIds  in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getBatchJobNotificationEmailIds() {
        akritiv__SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String emailIds = 'admin@akritiv.com';

        if(sysConfObj!=null) {
            if(sysConfObj.akritiv__Batch_Job_Notification_Emails__c !=null) {
                emailIds = sysConfObj.akritiv__Batch_Job_Notification_Emails__c;
            }
        }
        return emailIds;
    } 
    
       // Fetches the EmailID which is specified in BatchJobNotificationEmailId  in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getBatchJobNotificationEmailId() {
        akritiv__SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String emailId = 'admin@akritiv.com';

        if(sysConfObj!=null) {
            if(sysConfObj.akritiv__Batch_Job_Notification_Email__c !=null) {
                emailId = sysConfObj.akritiv__Batch_Job_Notification_Email__c;
            }
        }
        return emailId;
    }
    
     // -------------- Trigger Conguration ------------------
    // Returns default  value of Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static akritiv__Trigger_Configuration__c getTriggerConfigObj() {

        return akritiv__Trigger_Configuration__c.getOrgDefaults();
    }
    
     // Returns After Insert Full DataLoad Batch specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getAfterInsertFullDataLoadBatch() {
        return getTriggerConfigObj().After_Insert_Full_Data_Batch_Test__c;
    }
    
    // Fetches Default values specified in Batch Job Configuration Custom Setting
    // Return Type - SysConfig_Singlec__C 
    // No parameters are passed in this method 
    public static akritiv__BatchJobConfiguration_Singlec__c getBatchConfigObj() {
        if(akritiv__BatchJobConfiguration_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            return akritiv__BatchJobConfiguration_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(akritiv__BatchJobConfiguration_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            return akritiv__BatchJobConfiguration_Singlec__c.getInstance(Userinfo.getUserid());
        }

        return akritiv__BatchJobConfiguration_Singlec__c.getOrgDefaults();
    }
   
   
   // Fetches the EmailIDs which is specified in BatchJobNotificationEmailIds  in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getAutoCloseServiceNotification() {
        akritiv__BatchJobConfiguration_Singlec__c batchConfObj = ConfigUtilController.getBatchConfigObj();
        String emailId = 'admin@akritiv.com';

        if(batchConfObj !=null) {
            if(batchConfObj.akritiv__Auto_Close_Email_Service__c !=null) {
                emailId = batchConfObj.akritiv__Auto_Close_Email_Service__c;
            }
        }
        return emailId;
    } 
   
    
    
    
}