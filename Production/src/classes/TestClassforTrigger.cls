@isTest
private class TestClassforTrigger {

     static testmethod void testAfterInsertCreditReview2(){
    akritiv__SysConfig_Singlec__c sysConfig = new akritiv__SysConfig_Singlec__c();
    
    sysConfig.akritiv__Billing_Statement_Pdf_Template__c = 'TestPdfTemplateName';
    sysConfig.akritiv__Billing_Statement_Excel_Template__c = 'TestExcelTemplateName';
    sysConfig.akritiv__Org_Logo__c = 'Some Url to image';
    sysConfig.akritiv__Billing_Statement_PDF_page__c = 'Test';
    sysConfig.akritiv__NameSpace__c = 'akritiv';
    sysConfig.akritiv__Default_Currency__c = '$';
    sysConfig.akritiv__Billing_Statement_PDF_page__c = 'testPdfPage';
    sysConfig.akritiv__Billing_Statement_Excel_page__c = 'testExcelPage';
    sysConfig.akritiv__Batch_Job_Notification_Email__c = 'test@test.com';
    upsert sysConfig;
    
    account ac = new account();
    ac.name = 'test';
    ac.akritiv__Account_Key__c = 'testkey';
    ac.Exp_Combined_Account_Balances_60_Day__c = 60;
    insert ac;
    
    akritivcredit__Credit_Review__c testCredit = new akritivcredit__Credit_Review__c();
    testCredit.akritivcredit__Account__c = ac.id;
    insert testCredit;
    
    akritiv__Trigger_Configuration__c triggerConf = new akritiv__Trigger_Configuration__c();
    //triggerConf.After_Insert_Full_DataLoad_Batch_GE__c = true;

    insert triggerConf;
       

}
}