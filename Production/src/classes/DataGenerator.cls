/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for Account insert,payment insert,
            paymentline insert , task insert , contact insert etc.
 */
public class DataGenerator
{
    // method is used for account insert 
    // return type : Account
    public static Account accountInsert()
    {
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.akritiv__Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.akritiv__Sticky_Note__c = 'Add Account Notes';
        accObj.akritiv__Sticky_Note_Level__c = 'Normal';
        insert accObj;
        return accObj;
    }
    
    // method is used for Contact insert 
    // return type : Contact
    // parameters : acc - Account
    public static Contact contactInsert(Account acc)
    {
        // create a related contact to test account

        Contact contact = new Contact();
        contact.LastName = 'TestContact';
        contact.Email = 'test@test.com';
        contact.AccountId = acc.Id;
        insert contact;
        return contact;

    }
    
    // method is used for transaction Insert 
    // return type : List<Transaction__c>
    // parameters : acc - Account
    // Parameter: count - number of trnsactions inserted.
    public static List<akritiv__Transaction__c> transactionInsert(Account acc, Integer count)
    {
        //create transaction on Account

        List<akritiv__Transaction__c> txObjList = new List<akritiv__Transaction__c>();
        List<Id> idLst = new  List<Id>();

        Account tempAcc = acc;

        for(Integer i=0; i<count; i++)
        {
            akritiv__Transaction__c txObj = new akritiv__Transaction__c();
            txObj.akritiv__Account__c = tempAcc.Id;
            txObj.akritiv__Amount__c = 12312 + i;
            txObj.akritiv__Balance__c = 6336 + i;
            txObj.Name = 'testno' + i;
            txObj.akritiv__Due_Date__c = Date.today().addMonths(-7);
            txObj.akritiv__Close_Date__c = Date.today() +10;
            txObj.akritiv__Promised_Amount__c = 5000;
            txObj.akritiv__Disputed_Amount__c = 23000;
            txObj.akritiv__Create_Date__c = Date.today().addMonths(-9);
            txObj.akritiv__Bill_To_City__c = 'xyz';
            txObj.akritiv__Ship_To_City__c='xyz';
            txObj.akritiv__Bill_To_State__c = 'Ohio';
            txObj.akritiv__Ship_To_State__c = 'Ohio';
            txObj.akritiv__Bill_To_Country__c ='US';
            txObj.akritiv__Ship_To_Country__c ='US';
            //txObj.akritiv__Transaction_Key__c = 'TRX' + System.now().format('yyyyMMddHHmmss');
            txObjList.add(txObj);
        }

        insert txObjList;

        return txObjList;
    }
    
    
    // method is used for transaction Insert 
    // return type : List<Transaction__c>
    // parameters : acc - Account
    public static List<akritiv__Transaction__c> transactionInsert(Account acc)
    {
        return transactionInsert(acc, 5);
    }
    
    // method is used for dispute Insert 
    // return type : List<Dispute__c>
    // parameters : acc - Account , tx - List < Transaction__c >
    public static List<akritiv__Dispute__c> disputeInsert(Account acc, List<akritiv__Transaction__c> tx)
    {
        //create dispute on tx
        if(tx != null)
        {
            List<akritiv__Dispute__c> tempDisp = new List<akritiv__Dispute__c>();
            for(akritiv__Transaction__c tempTx : tx)
            {
                akritiv__Dispute__c dispute = new akritiv__Dispute__c();
                dispute.akritiv__Amount__c = tempTx.akritiv__Balance__c;
                dispute.akritiv__Transaction__c = tempTx.Id;
                dispute.akritiv__Account__c = acc.Id;
                tempDisp.add(dispute);
            }
            insert tempDisp;
            return tempDisp;
        }
        else
        {
            List<akritiv__Dispute__c> tempDisp = new List<akritiv__Dispute__c>();
            akritiv__Dispute__c dispute = new akritiv__Dispute__c();
            dispute.akritiv__Amount__c = 1234;
            dispute.akritiv__Account__c = acc.Id;
            tempDisp.add(dispute);
            insert tempDisp;
            return tempDisp;
        }

    }
    
    // method is used for lineItem Insert 
    // return type : List<Line_Item__c>
    // parameters : acc - Account , tx - List<Transaction__c>
    public static List<akritiv__Line_Item__c> lineItemInsert(Account acc, List<akritiv__Transaction__c> tx){
        String txIds='';
        List<akritiv__Line_Item__c > lineItemList = new List<akritiv__Line_Item__c >();
        if(tx != null)
        {
            for(akritiv__Transaction__c nextTx : tx ) {
                txIds=txIds + nextTx.Id + ',';
                //add line items
                akritiv__Line_Item__c lineItemObj = new akritiv__Line_Item__c();
                lineItemObj.Name ='testitem';
                lineItemObj.akritiv__Line_Total__c =10.0;
                lineItemObj.akritiv__Transaction__c = nextTx.Id;
                lineItemList.add(lineItemObj);
            }
            insert lineItemList;
        }
        return lineItemList;
    }
    
    // method is used for tempObject Insert 
    // return type : String
    // parameters : acc - Account , tx List < Transaction__c >
    public static String tempObjectInsert(Account acc, List<akritiv__Transaction__c> tx){
        String txIds='';
        if(tx != null)
        {
            for(akritiv__Transaction__c nextTx : tx ) {
                txIds=txIds + nextTx.Id + ',';
            }
        }

        String key = Userinfo.getUserName()+Datetime.now();
        akritiv__Temp_Object_Holder__c txIdsConfig = new akritiv__Temp_Object_Holder__c();
        txIdsConfig.akritiv__key__c = key;
        txIdsConfig.akritiv__value__c = txIds;

        insert (txIdsConfig);

        return key;

    }
    
    // method is used for task Insert 
    // return type : Task
    // parameters : acc - Account
    public static Task taskInsert( Account acc) {
        //create a Task

        Task taskObj = new Task();
        taskObj.WhatId = acc.Id;
        taskObj.ActivityDate  = Date.today();

        insert taskObj;
        return taskObj;
    }
    
    // method is used for activity transaction Insert 
    // return type : List<Activity_Transaction__c>
    // parameters : acc - Account , t - Task
    public static List<akritiv__Activity_Transaction__c> activityTransactionInsert ( Account acc, Task t) {

        List<akritiv__Activity_Transaction__c> txObjList = new List<akritiv__Activity_Transaction__c>();

        for(Integer i=0; i<5; i++) {
            akritiv__Activity_Transaction__c txObj = new akritiv__Activity_Transaction__c();

            txObj.akritiv__Task_Id__c = t.Id;
            txObj.akritiv__Transaction_ids__c = acc.Id;

            txObjList.add(txObj);
        }

        insert txObjList;

        return txObjList;

    }
    
    // method is used for payment Insert 
    // return type : Payment__c
    // parameters : acc - Account , amount - Integer
    public static akritiv__Payment__c paymentInsert(Account acc, Integer amount )
    {
        //create payment on Account

        List<akritiv__Payment__c> payObjList = new List<akritiv__Payment__c>();

        Account tempAcc = acc;

        akritiv__Payment__c payObj = new akritiv__Payment__c();

        payObj.akritiv__Account__c = tempAcc.Id;
        payObj.akritiv__Amount__c = amount;

        insert payObj;
        return payObj;
    }

    // method is used for payment line Insert 
    // return type : Payment_Line__c
    // parameters : amount - Integer , payment - Payment__c , acc - Account , tx - Transaction__c
    public static akritiv__Payment_Line__c  paymentlineInsert(Integer amount, akritiv__Payment__c payment, Account acc, akritiv__Transaction__c tx){
        String txIds='';
        Account tempAcc = acc;

        //add line items
        akritiv__Payment_Line__c paylineObj = new akritiv__Payment_Line__c();
        paylineObj.akritiv__Applied_Amount__c =amount;
        paylineObj.akritiv__Applied_To__c =tx.Id;
        paylineObj.akritiv__Payment__c =payment.Id;

        insert paylineObj;

        return paylineObj;

    }

    // method is used for opportunity Insert 
    // return type : Opportunity
    // parameters : a - Account
    public static Opportunity insetOpportunity( Account a ) {

        /* Opportunity o = new Opportunity();
        o.CloseDate = Date.today();
        o.StageName = 'Qualification';
        o.Name = 'Test Opportunity';
        o.AccountId = a.Id;

        insert o;

        PricebookEntry entry = [ Select p.UseStandardPrice, p.UnitPrice, p.SystemModstamp, p.ProductCode, p.Product2Id, p.Pricebook2Id, p.Name, p.LastModifiedDate, p.LastModifiedById, p.IsDeleted, p.IsActive, p.Id, p.CreatedDate, p.CreatedById From PricebookEntry p limit 1];

        List < OpportunityLineItem > lst = new List < OpportunityLineItem >();
        for ( Integer i = 0; i< 5; i++ ) {

            OpportunityLineItem item = new OpportunityLineItem();
            item.OpportunityId = o.Id;
            item.Quantity = 100;
            item.UnitPrice = entry.UnitPrice;

            item.Description = 'Test';
            item.PricebookEntryId = entry.Id;
            lst.add(item);

        }

        insert lst;
        return o; */
        return null;

    }

}