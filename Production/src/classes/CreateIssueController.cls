public class CreateIssueController
{
    public String username {get;set;}
    public String password {get;set;}
    public String currentUrl {get;set;}
    public CreateIssueController()
    {               
        String emailAddress = UserInfo.getUserEmail();
        System.debug('------emailAddress------'+emailAddress);
        List<Akritiv_Support_Configuration__c> lstUser = [Select Name,Username__c,Password__c,Email_Address__c,isDefault__c From Akritiv_Support_Configuration__c where Email_Address__c =: emailAddress limit 1];
        if(lstUser.size() > 0){
            username = lstUser[0].Username__c;
            password = lstUser[0].Password__c;
        }
        else{
            List<Akritiv_Support_Configuration__c> lstDefaultUser = [Select Name,Username__c,Password__c,Email_Address__c,isDefault__c From Akritiv_Support_Configuration__c where isDefault__c =:true limit 1];
            if(lstDefaultUser.size() > 0){
               username = lstDefaultUser[0].Username__c;
               password = lstDefaultUser[0].Password__c;           
            }else{
             ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Warning, 'Please Configure your Akritiv Support Configuration'));
             }
        }
    }
}