/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/**************************************************************************
   Name    :   TestAutomatedBatchCloseProcess
   Author  :   Hitesh Patel
   Date    :   July 15, 2010
   Usage   :   Test Automated Batch Close Balance Process
 ****************************************************************************/
@isTest
private class TestAutomatedBatchCloseProcessTest {
   
     
        static testMethod void case2() {
         list<User> listu = [Select id from user where Profile.name='System Administrator' and isActive=true limit 1];
        User u=listu.get(0);
        System.RunAs(u){
        // Test.startTest();
          akritiv__Trigger_Configuration__c systconfig = akritiv__Trigger_Configuration__c.getOrgDefaults();
         systconfig.akritiv__Zero_Balance_Transaction_Trigger__c = false;
         upsert systconfig;
        List<akritiv__Transaction__c> txList = new List<akritiv__Transaction__c>();
        
        //Account Insert
        Account tempAccount = DataGenerator.accountInsert();
        
       // List<Data_Load_Batch__c> dataloadbatchList = new List<Data_Load_Batch__c>();
        akritiv__SysConfig_Singlec__c sconf = akritiv__SysConfig_Singlec__c.getOrgDefaults();
        sconf.akritiv__BucketsOnBase__c = true;
        sconf.akritiv__Billing_Statement_PDF_page__c ='test2';
        upsert sconf;
        
        akritiv__Data_Load_Batch__c dataloadObj = new akritiv__Data_Load_Batch__c();
        dataloadObj.akritiv__Source_System__c = 'SAP';
        dataloadObj.akritiv__Batch_Number__c = '20102503';
       // dataloadObj.Load_Type__c = 'FULL';
        dataloadObj.akritiv__Run_Batch_Aging__c = false;
        insert dataloadObj;
        
       
      //  Id batchinfoId = dataloadbatchList[0].ID;
        //List of tx on Account
        
        txList = DataGenerator.transactionInsert(tempAccount, 1);
        txList.get(0).akritiv__Transaction_Key__c = 'testingkey-11111300';
        txList.get(0).akritiv__Source_System__c = 'SAP';
        txList.get(0).akritiv__Batch_Number__c = '20102503';
         txList.get(0).akritiv__Base_Balance__c = 2000;
        update txList;
        Test.startTest();

        AutomatedBatchCloseProcessTest autoloadcloseObj = new AutomatedBatchCloseProcessTest();
        akritiv__Transaction__c tx = [ Select a.akritiv__Transaction_Key__c, a.akritiv__Source_System__c, a.akritiv__Batch_Number__c From akritiv__Transaction__c a where akritiv__Transaction_Key__c <> null and akritiv__Source_System__c  <> null and akritiv__Source_System__c <>'' and akritiv__Batch_Number__c  <> null and akritiv__Batch_Number__c <>'' order by akritiv__Batch_Number__c desc limit 1];
        autoloadcloseObj.latestbatch = tx.akritiv__Batch_Number__c;
        autoloadcloseObj.sourcesystem = tx.akritiv__Source_System__c;
        autoloadcloseObj.batchInfoId=null;
     
        autoloadcloseObj.start(null);
      
        autoloadcloseObj.execute(null, txList);
        autoloadcloseObj.closetotal = 25;
        autoloadcloseObj.opentotal= 4000;
        
        autoloadcloseObj.finish(null);
        
        autoloadcloseObj.closetotal = 343434;
        autoloadcloseObj.opentotal= 5;
        System.assertEquals(autoloadcloseObj.closetotal,343434);
         try{
            autoloadcloseObj.finish(null);
        }catch(Exception e) {
            autoloadcloseObj.ex = e;
            autoloadcloseObj.flag = true;
            autoloadcloseObj.sendMail();
            return;
            
        }
     
        Test.stopTest();
        }
    }
     static testMethod void case3() {
         list<User> listu = [Select id from user where Profile.name='System Administrator' and isActive=true limit 1];
        User u=listu.get(0);
        System.RunAs(u){
        // Test.startTest();
          akritiv__Trigger_Configuration__c systconfig = akritiv__Trigger_Configuration__c.getOrgDefaults();
         systconfig.akritiv__Zero_Balance_Transaction_Trigger__c = false;
         upsert systconfig;
        List<akritiv__Transaction__c> txList = new List<akritiv__Transaction__c>();
        
        //Account Insert
        Account tempAccount = DataGenerator.accountInsert();
        
       // List<Data_Load_Batch__c> dataloadbatchList = new List<Data_Load_Batch__c>();
        akritiv__SysConfig_Singlec__c sconf = akritiv__SysConfig_Singlec__c.getOrgDefaults();
        sconf.akritiv__BucketsOnBase__c = false;
        sconf.akritiv__Billing_Statement_PDF_page__c ='test2';
        upsert sconf;
        
        akritiv__Data_Load_Batch__c dataloadObj = new akritiv__Data_Load_Batch__c();
        dataloadObj.akritiv__Source_System__c = 'SAP';
        dataloadObj.akritiv__Batch_Number__c = '20102503';
       // dataloadObj.Load_Type__c = 'FULL';
        dataloadObj.akritiv__Run_Batch_Aging__c = false;
        insert dataloadObj;
        
       
      //  Id batchinfoId = dataloadbatchList[0].ID;
        //List of tx on Account
        
        txList = DataGenerator.transactionInsert(tempAccount, 1);
        txList.get(0).akritiv__Transaction_Key__c = 'testingkey-11111300';
        txList.get(0).akritiv__Source_System__c = 'SAP';
        txList.get(0).akritiv__Batch_Number__c = '20102503';
         txList.get(0).akritiv__Base_Balance__c = 2000;
        update txList;
        Test.startTest();

        AutomatedBatchCloseProcessTest autoloadcloseObj = new AutomatedBatchCloseProcessTest();
        akritiv__Transaction__c tx = [ Select a.akritiv__Transaction_Key__c, a.akritiv__Source_System__c, a.akritiv__Batch_Number__c From akritiv__Transaction__c a where akritiv__Transaction_Key__c <> null and akritiv__Source_System__c  <> null and akritiv__Source_System__c <>'' and akritiv__Batch_Number__c  <> null and akritiv__Batch_Number__c <>'' order by akritiv__Batch_Number__c desc limit 1];
      // autoloadcloseObj.latestbatch = tx.akritiv__Batch_Number__c;
        autoloadcloseObj.sourcesystem = tx.akritiv__Source_System__c;
        autoloadcloseObj.batchInfoId=dataloadObj.Id;
     
        autoloadcloseObj.start(null);
      
        autoloadcloseObj.execute(null, txList);
        autoloadcloseObj.closetotal = 25;
        autoloadcloseObj.opentotal= 4000;
        
        autoloadcloseObj.finish(null);
        
        autoloadcloseObj.closetotal = 343434;
        autoloadcloseObj.opentotal= 5;
        System.assertEquals(autoloadcloseObj.closetotal,343434);
         try{
            autoloadcloseObj.finish(null);
        }catch(Exception e) {
            autoloadcloseObj.ex = e;
            autoloadcloseObj.flag = true;
            autoloadcloseObj.sendMail();
            return;
            
        }
     
        Test.stopTest();
        }
    }
    
    public class MyException extends Exception {}
}