trigger BeforeCreateDisputeAssignDM on akritiv__Dispute__c (before insert) {
    akritiv__Trigger_Configuration__c tc = akritiv__Trigger_Configuration__c.getOrgDefaults(); 
    boolean accbin = tc.BeforeCreateDisputeAssignDM__c;
    
    Map<Id,String> disTypeMap = new  Map<Id,String>();
    Map<String,Id> disDMTypeMap = new  Map<String,Id>();
    
    List<String> disTypeList = new List<String>();
    List<Dispute_Matrix__c> disMatList = new List<Dispute_Matrix__c>();
    
    if(accbin){ 
        for(akritiv__Dispute__c d : Trigger.new){
            if(d.akritiv__Type__c != null && d.akritiv__Type__c != ''){
                disTypeMap.put(d.Id , d.akritiv__Type__c);
                disTypeList.add(d.akritiv__Type__c);
            }
        }
        
        if(disTypeList.size() > 0){
            disMatList = [Select Id,Type__c from Dispute_Matrix__c where Type__c in : disTypeList];

            if(disMatList.size() > 0){
                
                for(Dispute_Matrix__c dm : disMatList){
                    if(dm.Type__c != null && dm.Type__c != ''){
                        disDMTypeMap.put(dm.Type__c , dm.Id);
                    }
                }
                
                for(akritiv__Dispute__c d1 : Trigger.new){
                    if(d1.akritiv__Type__c != null && d1.akritiv__Type__c != ''){
                        d1.Dispute_Matrix__c = disDMTypeMap.get(disTypeMap.get(d1.Id));
                    }
                }
                
            }
        }
    }
}