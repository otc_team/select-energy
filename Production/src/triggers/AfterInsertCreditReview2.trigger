trigger AfterInsertCreditReview2 on akritivcredit__Credit_Review__c ( after insert , before update ) {


       Set < Id > accountIds = new Set< Id >();
       Map < Id , Account > actoup = new Map < Id , Account >();
       for ( akritivcredit__Credit_Review__c c : Trigger.New ) {
                  
              accountIds.add( c.akritivcredit__Account__c);
       }
       
       Set < Account > accountsToUpdate = new Set < Account >();

       if ( Trigger.isInsert ){
       
           Map < Id , Account > accountMap = new Map < Id, Account >([select Id ,akritivcredit__Latest_Credit_Review__c ,akritiv__Credit_Limit__c from Account where Id in: accountIds]);
           
           for ( akritivcredit__Credit_Review__c c : Trigger.New ) {
               
                   Account a = accountMap.get( c.akritivcredit__Account__c);
                   if(a == null){
                       continue;
                   }
                   
                   
                   if ( actoup.get( a.Id ) == null ){
                    
                     a.akritivcredit__Latest_Credit_Review__c = c.Id;
                     actoup.put( a.Id, a );
                     accountsToUpdate.add( a );   
                   }
                   
               
           }
           
            List < Account> accountlst = new List< Account>();
            for(Account a:accountsToUpdate ){
                accountlst.add(a);
            }
           
           upsert accountlst ;
           
       } else {
       
           Map < Id , Account > accountMap = new Map < Id, Account >([select Id ,akritiv__Last_Credit_Review_Date__c,akritiv__Credit_Limit__c, akritivcredit__Latest_Credit_Review__c from Account where Id in: accountIds]);
           
           for ( akritivcredit__Credit_Review__c c : Trigger.New ) {
           
               Account a = accountMap.get( c.akritivcredit__Account__c);
               
               if(a == null){
                       continue;
               }
               a.akritivcredit__Latest_Credit_Review__c = c.Id;
               a.akritiv__Last_Credit_Review_Date__c = Date.today();
               //c.System_Suggested_Credit_Limit__c = c.Credit_Calculation__c;
               System.debug('status in trigger ' +c.akritivcredit__Status__c );
               if (c.akritivcredit__Completed__c == true) {
    
                //c.Approved_Credit_Limit__c = c.System_Suggested_Credit_Limit__c;
                
                   if(c.akritivcredit__Approved_Credit_Limit__c != null){
                        //double tempCreditLimit = c.Approved_Credit_Limit__c/conversionRateMap.get(c.CurrencyISOCode);
                        //a.akritiv__Credit_Limit__c = tempCreditLimit * conversionRateMap.get(a.CurrencyISOCode);
                        a.akritiv__Credit_Limit__c = c.akritivcredit__Approved_Credit_Limit__c;
                       
                    }
                }
               
               if ( actoup.get( a.Id ) == null ){
                     actoup.put( a.Id, a );
                     accountsToUpdate.add( a );   
               }
               
               
           }
           
           List < Account> accountlst = new List< Account>();
            for(Account a1:accountsToUpdate ){
                accountlst.add(a1);
            }
           
           upsert accountlst ;
           
       
       }
}