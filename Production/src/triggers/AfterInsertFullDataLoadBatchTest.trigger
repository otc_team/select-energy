/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Used to run FUll Batch Load for all transactions
 */
trigger AfterInsertFullDataLoadBatchTest on akritiv__Data_Load_Batch__c (after insert, before update) {
    
    
    
    if(ConfigUtilController.getAfterInsertFullDataLoadBatch()){
        if(Trigger.isInsert) {
            String templateName = 'SYSTEM Data Load Fail Notification';
            EmailTemplate template = [select Name,HtmlValue from EmailTemplate where Name =: templateName limit 1];
            for(akritiv__Data_Load_Batch__c dlb: Trigger.new) {
                if(dlb.akritiv__Load_Type__c == 'FULL') {
                   
                   
                    AutomatedBatchCloseProcessTest bafl = new AutomatedBatchCloseProcessTest();
                    bafl.latestbatch = dlb.akritiv__Batch_Number__c;
                    bafl.sourcesystem = dlb.akritiv__Source_System__c;
                    bafl.batchInfoId = dlb.Id;
                    Database.executeBatch(bafl , 1);
                   
                  
                }else{
                    
                    if(dlb.akritiv__Result__c != null && dlb.akritiv__Result__c.equalsIgnoreCase('ERROR')){
                              String addresses = ConfigUtilController.getBatchJobNotificationEmailIds();
                              String [] toAddress = addresses.split(',');
                              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
                            
                            mail.setToAddresses(toAddress);
                            //mail.setReplyTo(toAddress);
                            mail.setSenderDisplayName('Data Load Failed :');
                            
                            mail.setSubject( 'Data Load Failed'+' For '+ Userinfo.getOrganizationName());
                            
                            //String mailBodyText = 'Data load failed due to error in Data load.';
                            
                            String mailBody = template.HtmlValue;
                            mailBody = mailBody.replace('{!organizationname}',Userinfo.getOrganizationName());
                            mailBody = mailBody.replace('{!notificationdate}',Date.Today()+'');
                            
                            mailBody = mailBody.replace('{!loadtype}',dlb.akritiv__Load_Type__c);
                            mailBody = mailBody.replace('{!result}',dlb.akritiv__Result__c);
                            
                            mailBody = mailBody.replace('{!batchnumber}',dlb.akritiv__Batch_Number__c);
                            mailBody = mailBody.replace('{!sourcesystem}',dlb.akritiv__Source_System__c);
                            
                            mail.setHtmlBody(mailBody);
                            
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                       }
                       
                    if ( dlb.akritiv__Run_Batch_Aging__c ) {
                      //  InvokeBatchAgingCalculations.startBatch();  
                      //  InvokeBatchAgingCalculations.startADPDBatch();
                    }
                }
            }
        }
    
    }   
 
}